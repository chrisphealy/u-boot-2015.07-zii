/*
 * Copyright 2014 Toradex AG
 *
 * SPDX-License-Identifier:     GPL-2.0+ or X11
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/dts-v1/;
#include "vf.dtsi"

/ {
	model = "Zii SPU3";
	compatible = "toradex,vf610-colibri_vf61", "toradex,vf610-colibri_vf61", "fsl,vf610", "fsl,vf610twr";
};

&dspi1 {
	status = "okay";
	bus-num = <0>;

	spi_nor0: dspi1@cs0 {
		reg = <0>;
		compatible = "spi-flash";
		spi-max-frequency = <50000000>;
	};
};

&i2c0 {
	clock-frequency = <100000>;
	status = "okay";

	temp_sensor: lm75@48 {
		compatible = "national,lm75";
		reg = <0x48>;
	};

	io_expander: pca9554@20 {
		compatible = "nxp,pca9554";
		reg = <0x20>;
	};

	eeprom_nvm: at24c04@50 {
		compatible = "atmel,24c04";
		reg = <0x50>;
	};

	eeprom_nameplate: at24c04@52 {
		compatible = "atmel,24c04";
		reg = <0x52>;
	};

	etc: ds1682@6b {
		compatible = "dallas,ds1682";
		reg = <0x6b>;
	};
};

