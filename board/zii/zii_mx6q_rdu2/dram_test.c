#include "dram_test.h"

static int dram_test_done = 0;           /*will be env variable*/

/**********************************************************************
 *
 * Function:    memtest_data_bus()
 * Description: Walking 1's on memroy data bus.
 * Returns:     0 = success
 *              non-zero on first failed pattern
 *
 **********************************************************************/
unsigned long memtest_data_bus (volatile unsigned long * addr) {
    unsigned long pat;

    for (pat = 1; pat != 0; pat <<= 1) {
        *addr = pat;
        *(addr+1) = ~pat;
        if (*addr != pat) {
            return pat;
        }
    }

    return 0;
}


/**********************************************************************
 *
 * Function:    memtest_address_bus()
 * Description: Walking 1's on memroy address bus.
 * Returns:     0 = success
 *              non-zero on first failed address
 *
 **********************************************************************/
unsigned long memtest_address_bus (volatile unsigned long * addr, unsigned long length) {
    unsigned long mask, offsetA, offsetB;
    unsigned long patA = 0xAAAAAAAA;
    unsigned long patB = ~patA;
     
    mask = length/sizeof(unsigned long) -1;
    /* write pattern A */
    for (offsetA = 1; (offsetA & mask); offsetA <<= 1) {
        addr[offsetA] = patA;
    }

    offsetB = 0;
    addr[offsetB] = patB;
    /* test for address bits stuck '1' */ 
    for (offsetA = 1; (offsetA & mask); offsetA <<= 1) {
        if (addr[offsetA] != patA) {
            return (unsigned long)&addr[offsetA];
        }
    }

    addr[offsetB] = patA;
    /* test for address bits stuck '0' */
    for (offsetB = 1; (offsetB & mask); offsetB <<= 1) {
        addr[offsetB] = patB;
        if (addr[0] != patA) {
            return (unsigned long)&addr[offsetB];
        }
        for (offsetA = 1; (offsetA & mask); offsetA <<= 1) {
            if ((addr[offsetA] != patA) && (offsetA != offsetB)) {
                return (unsigned long)&addr[offsetB];
            }
        }
        
        addr[offsetB] = patA; 
    }

    return 0;
}

/**********************************************************************
 *
 * Function:    memtest_all()
 * Description: toggle 1's and 0's at all bit location for entire chip
 * Returns:     0 = success
 *              non-zero on first failed address
 *
 **********************************************************************/
unsigned long memtest_all (volatile unsigned long * addr, unsigned long length) {
    unsigned long numWords, offsetA, offsetB;
    unsigned long patA, patB;
    
    patA = 1; 
    numWords = length/sizeof(unsigned long);
    /* write pattern A */
    for (offsetA = 0; offsetA < numWords; offsetA++) {
        addr[offsetA] = patA++;
    }

    patA = 1;
    /* read and verify each location for pattern A */ 
    for (offsetA = 0; offsetA < numWords; offsetA++) {
        if (addr[offsetA] != patA) {
            return (unsigned long)&addr[offsetA];
        }
        patB = ~patA;
        /* write pattern B for second pass */
        addr[offsetA] = patB;
        patA++;
    }

    patA = 1;
    /* read and verify each location for pattern B */
    for (offsetB = 0; offsetB < numWords; offsetB++) {
        patB = ~patA;
        if (addr[offsetB] != patB) {
            return (unsigned long)&addr[offsetB];
        }
        patA++;    
    }

    return 0;
}
 
/**********************************************************************
 *
 * Function:    t_dram_test()
 * Description: test dram chip. This function will execute from OCRAM
 * Returns:     0 = success
 *              non-zero on first error (data,address or chip)
 *
 **********************************************************************/
unsigned long t_dram_test (void) {

    if (memtest_data_bus ((volatile unsigned long*)DRAM_TEST_START_ADDR)) {
        return ERR_DRAM_TEST_DATA_BUS;
    }

    if (memtest_address_bus ((volatile unsigned long*)DRAM_TEST_START_ADDR, DRAM_TEST_NUM_BYTES) )  {
        return ERR_DRAM_TEST_ADDR_BUS;
    }

    if (!dram_test_done) {
        if (memtest_all ((volatile unsigned long*)DRAM_TEST_START_ADDR, DRAM_TEST_NUM_BYTES)) {
            return ERR_DRAM_TEST_CHIP;
        }
    }

    /* set env variable to skip long test */
    /* dram_test_done = 1 */

    return 0;
}
