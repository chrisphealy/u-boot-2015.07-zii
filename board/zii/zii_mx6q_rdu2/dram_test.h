
/**********************************************************************
 *
 * Function:    memtest_data_bus()
 * Description: Walking 1's on memroy data bus.
 * Returns:     0 = success
 *              non-zero on first failed pattern
 *
 **********************************************************************/
unsigned long memtest_data_bus (volatile unsigned long * addr);

/**********************************************************************
 *
 * Function:    memtest_address_bus()
 * Description: Walking 1's on memroy address bus.
 * Returns:     0 = success
 *              non-zero on first failed address
 *
 **********************************************************************/
unsigned long memtest_address_bus (volatile unsigned long * addr, unsigned long length);

/**********************************************************************
 *
 * Function:    memtest_all()
 * Description: toggle 1's and 0's at all bit location for entire chip
 * Returns:     0 = success
 *              non-zero on first failed address
 *
 **********************************************************************/
unsigned long memtest_all (volatile unsigned long * addr, unsigned long length);

#define DRAM_TEST_START_ADDR  0x10000000
#define DRAM_TEST_NUM_BYTES   0x80000000 /*2GB*/

enum {
    ERR_DRAM_TEST_DATA_BUS = 1,  /* data lines wiring */
    ERR_DRAM_TEST_ADDR_BUS = 2,  /* address lines wiring*/
    ERR_DRAM_TEST_CHIP = 3,      /* DRAM chip errors */
};

 
/**********************************************************************
 *
 * Function:    t_dram_test()
 * Description: test dram chip. This function will execute from OCRAM
 * Returns:     0 = success
 *              non-zero on first error (data,address or chip)
 *
 **********************************************************************/
unsigned long t_dram_test (void);
