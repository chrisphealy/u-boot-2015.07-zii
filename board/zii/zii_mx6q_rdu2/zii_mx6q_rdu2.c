/*
 * Copyright (C) 2015 Zodiac Inflight Innovations
 *
 * Based on:
 * Copyright (C) 2012 Freescale Semiconductor, Inc.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/iomux.h>
#include <asm/arch/mx6-pins.h>
#include <asm/errno.h>
#include <asm/gpio.h>
#include <asm/imx-common/mxc_i2c.h>
#include <asm/imx-common/iomux-v3.h>
#include <asm/imx-common/boot_mode.h>
#include <asm/imx-common/video.h>
#include <mmc.h>
#include <fsl_esdhc.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/arch/mxc_hdmi.h>
#include <asm/arch/crm_regs.h>
#include <asm/io.h>
#include <asm/arch/sys_proto.h>
#include <i2c.h>
#include <power/pmic.h>
#include <power/pfuze100_pmic.h>
#include "pfuze.h"
#include <asm/arch/mx6-ddr.h>
#include <asm/imx-common/sata.h>
#include <usb.h>
#include <dm.h>
#include <dm/platform_data/serial_mxc.h>
#include <stdlib.h>
#include "../common/zii_platforms/zii_rdu2_lcd_type.h"
#include <fdt_support.h>

DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
	PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm |			\
	PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define USDHC_BI_DIR_PAD_CTRL (PAD_CTL_PUS_47K_UP |			\
	PAD_CTL_SPEED_LOW | PAD_CTL_DSE_48ohm |			\
	PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define USDHC_CLK_PAD_CTRL (PAD_CTL_PUS_47K_UP |			\
	PAD_CTL_SPEED_LOW | PAD_CTL_DSE_48ohm |			\
	PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define ENET_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
	PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm | PAD_CTL_HYS)

#define SPI_PAD_CTRL (PAD_CTL_HYS | \
 					PAD_CTL_SPEED_MED | \
 					PAD_CTL_DSE_40ohm)

#define SPI_CS_PAD_CTRL (PAD_CTL_HYS |	\
						PAD_CTL_PUS_22K_UP | \
						PAD_CTL_SPEED_MED | \
						PAD_CTL_DSE_40ohm)

#define I2C_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
	PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm | PAD_CTL_HYS |	\
	PAD_CTL_ODE | PAD_CTL_SRE_FAST)

#define I2C_PMIC	1

#define I2C_PAD MUX_PAD_CTRL(I2C_PAD_CTRL)

extern int rdu2_pic_get_display_type(void);
extern int rdu2_pic_send_display_data_stable(void);
extern void get_mx6_board_mac_addr(int nic_id, char mac_addr[6]);
extern void get_zii_part_number(void);


/*#define DISP0_PWR_EN	IMX_GPIO_NR(1, 21)*/

int dram_init(void)
{
	gd->ram_size = imx_ddr_size();
	return 0;
}

static iomux_v3_cfg_t const uart1_pads[] = {
	MX6_PAD_CSI0_DAT10__UART1_TX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL),
	MX6_PAD_CSI0_DAT11__UART1_RX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static iomux_v3_cfg_t const uart3_pads[] = {
	MX6_PAD_EIM_D24__UART3_TX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL),
	MX6_PAD_EIM_D25__UART3_RX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static iomux_v3_cfg_t const uart4_pads[] = {
	MX6_PAD_CSI0_DAT12__UART4_TX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL),
	MX6_PAD_CSI0_DAT13__UART4_RX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static iomux_v3_cfg_t const enet_pads[] = {
	MX6_PAD_ENET_MDIO__ENET_MDIO		| MUX_PAD_CTRL(ENET_PAD_CTRL),
	MX6_PAD_ENET_MDC__ENET_MDC			| MUX_PAD_CTRL(ENET_PAD_CTRL),

	MX6_PAD_GPIO_16__ENET_REF_CLK		| MUX_PAD_CTRL(ENET_PAD_CTRL),
	/*MX6_PAD_GPIO_16__GPIO7_IO11			| MUX_PAD_CTRL(NO_PAD_CTRL),*/
	MX6_PAD_ENET_CRS_DV__ENET_RX_EN		| MUX_PAD_CTRL(ENET_PAD_CTRL),
	MX6_PAD_ENET_RX_ER__ENET_RX_ER      | MUX_PAD_CTRL(ENET_PAD_CTRL),
	MX6_PAD_ENET_RXD0__ENET_RX_DATA0	| MUX_PAD_CTRL(ENET_PAD_CTRL),
	MX6_PAD_ENET_RXD1__ENET_RX_DATA1	| MUX_PAD_CTRL(ENET_PAD_CTRL),
	MX6_PAD_ENET_TX_EN__ENET_TX_EN		| MUX_PAD_CTRL(ENET_PAD_CTRL),
	MX6_PAD_ENET_TXD0__ENET_TX_DATA0	| MUX_PAD_CTRL(ENET_PAD_CTRL),
	MX6_PAD_ENET_TXD1__ENET_TX_DATA1	| MUX_PAD_CTRL(ENET_PAD_CTRL),
	/* PHY Reset */
	MX6_PAD_ENET_REF_CLK__GPIO1_IO23	| MUX_PAD_CTRL(NO_PAD_CTRL),
};

static void setup_iomux_enet(void)
{
	imx_iomux_v3_setup_multiple_pads(enet_pads, ARRAY_SIZE(enet_pads));

	/* Reset PHY */
	gpio_direction_output(IMX_GPIO_NR(1, 23) , 0);
	udelay(500);
	gpio_set_value(IMX_GPIO_NR(1, 23), 1);
}

static iomux_v3_cfg_t const usdhc2_pads[] = {
	MX6_PAD_SD2_CLK__SD2_CLK		| MUX_PAD_CTRL(USDHC_CLK_PAD_CTRL),
	MX6_PAD_SD2_CMD__SD2_CMD		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD2_DAT0__SD2_DATA0		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD2_DAT1__SD2_DATA1		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD2_DAT2__SD2_DATA2		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD2_DAT3__SD2_DATA3		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_NANDF_D2__GPIO2_IO02	| MUX_PAD_CTRL(NO_PAD_CTRL), /* CD */
};

static iomux_v3_cfg_t const usdhc3_pads[] = {
	MX6_PAD_SD3_CLK__SD3_CLK		| MUX_PAD_CTRL(USDHC_CLK_PAD_CTRL),
	MX6_PAD_SD3_CMD__SD3_CMD		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD3_DAT0__SD3_DATA0		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD3_DAT1__SD3_DATA1		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD3_DAT2__SD3_DATA2		| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD3_DAT3__SD3_DATA3 	| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_NANDF_D0__GPIO2_IO00	| MUX_PAD_CTRL(NO_PAD_CTRL), /* CD */
};

static iomux_v3_cfg_t const usdhc4_pads[] = {
	MX6_PAD_SD4_CLK__SD4_CLK	| MUX_PAD_CTRL(USDHC_CLK_PAD_CTRL),
	MX6_PAD_SD4_CMD__SD4_CMD	| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT0__SD4_DATA0	| MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT1__SD4_DATA1 | MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT2__SD4_DATA2 | MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT3__SD4_DATA3 | MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT4__SD4_DATA4 | MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT5__SD4_DATA5 | MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT6__SD4_DATA6 | MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
	MX6_PAD_SD4_DAT7__SD4_DATA7 | MUX_PAD_CTRL(USDHC_BI_DIR_PAD_CTRL),
};

static iomux_v3_cfg_t const ecspi1_pads[] = {
	MX6_PAD_EIM_D16__ECSPI1_SCLK | MUX_PAD_CTRL(SPI_PAD_CTRL),
	MX6_PAD_EIM_D17__ECSPI1_MISO | MUX_PAD_CTRL(SPI_PAD_CTRL),
	MX6_PAD_EIM_D18__ECSPI1_MOSI | MUX_PAD_CTRL(SPI_PAD_CTRL),
	/*MX6_PAD_EIM_EB2__ECSPI1_SS0  | MUX_PAD_CTRL(SPI_CS_PAD_CTRL),*/
	MX6_PAD_EIM_EB2__GPIO2_IO30	 | MUX_PAD_CTRL(SPI_CS_PAD_CTRL),
	/*MX6_PAD_KEY_COL2__ECSPI1_SS1 | MUX_PAD_CTRL(SPI_CS_PAD_CTRL),*/
	MX6_PAD_KEY_COL2__GPIO4_IO10 | MUX_PAD_CTRL(SPI_CS_PAD_CTRL),
};

static iomux_v3_cfg_t const rgb_pads[] = {
	MX6_PAD_DI0_DISP_CLK__IPU1_DI0_DISP_CLK | MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DI0_PIN15__IPU1_DI0_PIN15 		| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DI0_PIN2__IPU1_DI0_PIN02 		| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DI0_PIN3__IPU1_DI0_PIN03 		| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DI0_PIN4__IPU1_DI0_PIN04 		| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT0__IPU1_DISP0_DATA00	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT1__IPU1_DISP0_DATA01	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT2__IPU1_DISP0_DATA02	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT3__IPU1_DISP0_DATA03	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT4__IPU1_DISP0_DATA04	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT5__IPU1_DISP0_DATA05	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT6__IPU1_DISP0_DATA06	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT7__IPU1_DISP0_DATA07	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT8__IPU1_DISP0_DATA08	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT9__IPU1_DISP0_DATA09	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT10__IPU1_DISP0_DATA10	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT11__IPU1_DISP0_DATA11	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT12__IPU1_DISP0_DATA12	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT13__IPU1_DISP0_DATA13	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT14__IPU1_DISP0_DATA14	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT15__IPU1_DISP0_DATA15	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT16__IPU1_DISP0_DATA16	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT17__IPU1_DISP0_DATA17	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT18__IPU1_DISP0_DATA18	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT19__IPU1_DISP0_DATA19	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT20__IPU1_DISP0_DATA20	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT21__IPU1_DISP0_DATA21	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT22__IPU1_DISP0_DATA22	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_DISP0_DAT23__IPU1_DISP0_DATA23	| MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_SD1_DAT3__GPIO1_IO21			| MUX_PAD_CTRL(NO_PAD_CTRL),
};

static void enable_rgb(struct display_info_t const *dev)
{
	imx_iomux_v3_setup_multiple_pads(rgb_pads, ARRAY_SIZE(rgb_pads));
	/*gpio_direction_output(DISP0_PWR_EN, 1);*/
}

static struct i2c_pads_info i2c1_pad_info = {
	.scl = {
		.i2c_mode = MX6_PAD_CSI0_DAT9__I2C1_SCL | I2C_PAD,
		.gpio_mode = MX6_PAD_CSI0_DAT9__GPIO5_IO27 | I2C_PAD,
		.gp = IMX_GPIO_NR(5, 27)
	},
	.sda = {
		.i2c_mode = MX6_PAD_CSI0_DAT8__I2C1_SDA| I2C_PAD,
		.gpio_mode = MX6_PAD_CSI0_DAT8__GPIO5_IO26 | I2C_PAD,
		.gp = IMX_GPIO_NR(5, 26)
	}
};

static struct i2c_pads_info i2c2_pad_info = {
	.scl = {
		.i2c_mode = MX6_PAD_KEY_COL3__I2C2_SCL | I2C_PAD,
		.gpio_mode = MX6_PAD_KEY_COL3__GPIO4_IO12 | I2C_PAD,
		.gp = IMX_GPIO_NR(4, 12)
	},
	.sda = {
		.i2c_mode = MX6_PAD_KEY_ROW3__I2C2_SDA | I2C_PAD,
		.gpio_mode = MX6_PAD_KEY_ROW3__GPIO4_IO13 | I2C_PAD,
		.gp = IMX_GPIO_NR(4, 13)
	}
};

static struct i2c_pads_info i2c3_pad_info = {
	.scl = {
		.i2c_mode = MX6_PAD_GPIO_3__I2C3_SCL | I2C_PAD,
		.gpio_mode = MX6_PAD_GPIO_3__GPIO1_IO03 | I2C_PAD,
		.gp = IMX_GPIO_NR(1, 3)
	},
	.sda = {
		.i2c_mode = MX6_PAD_GPIO_6__I2C3_SDA | I2C_PAD,
		.gpio_mode = MX6_PAD_GPIO_6__GPIO1_IO06 | I2C_PAD,
		.gp = IMX_GPIO_NR(1, 6)
	}
};

static void setup_spi(void)
{
	imx_iomux_v3_setup_multiple_pads(ecspi1_pads, ARRAY_SIZE(ecspi1_pads));

	printf("Setting SPI1 Chip Selects High\n");
	gpio_direction_output(IMX_GPIO_NR(2, 30) , 1);
	gpio_direction_output(IMX_GPIO_NR(4, 10) , 1);
}

iomux_v3_cfg_t const pcie_pads[] = {
	MX6_PAD_EIM_D19__GPIO3_IO19 | MUX_PAD_CTRL(NO_PAD_CTRL),	/* POWER */
	MX6_PAD_GPIO_17__GPIO7_IO12 | MUX_PAD_CTRL(NO_PAD_CTRL),	/* RESET */
};

static void setup_pcie(void)
{
	imx_iomux_v3_setup_multiple_pads(pcie_pads, ARRAY_SIZE(pcie_pads));
}

iomux_v3_cfg_t const di0_pads[] = {
	MX6_PAD_DI0_DISP_CLK__IPU1_DI0_DISP_CLK,	/* DISP0_CLK */
	MX6_PAD_DI0_PIN2__IPU1_DI0_PIN02,		/* DISP0_HSYNC */
	MX6_PAD_DI0_PIN3__IPU1_DI0_PIN03,		/* DISP0_VSYNC */
};

static void setup_iomux_uart(void)
{
	imx_iomux_v3_setup_multiple_pads(uart1_pads, ARRAY_SIZE(uart1_pads));
	imx_iomux_v3_setup_multiple_pads(uart3_pads, ARRAY_SIZE(uart3_pads));
	imx_iomux_v3_setup_multiple_pads(uart4_pads, ARRAY_SIZE(uart4_pads));
}

/*
 * The following setups up all of the serial ports in the
 *  system.
 *  DO NOT CHANGE THE ORDER
 */

/* Serial Console */
static struct mxc_serial_platdata zii_rdu2_mxc_serial_console = {
	.reg = (struct mxc_uart *)UART1_BASE,
};

U_BOOT_DEVICE(zii_rdu2_serial_console) = {
	.name	= "serial_mxc",
	.platdata = &zii_rdu2_mxc_serial_console,
};

/* RS-485 Port */
static struct mxc_serial_platdata zii_rdu2_mxc_serial_rs485 = {
	.reg = (struct mxc_uart *)UART3_BASE,
};

U_BOOT_DEVICE(zii_rdu2_serial_rs485) = {
	.name	= "serial_mxc",
	.platdata = &zii_rdu2_mxc_serial_rs485,
};

/* PIC Communication */
static struct mxc_serial_platdata zii_rdu2_mxc_serial_pic = {
	.reg = (struct mxc_uart *)UART4_BASE,
};

U_BOOT_DEVICE(zii_rdu2_serial_pic) = {
	.name	= "serial_mxc",
	.platdata = &zii_rdu2_mxc_serial_pic,
};

#ifdef CONFIG_FSL_ESDHC
struct fsl_esdhc_cfg usdhc_cfg[3] = {
	{USDHC2_BASE_ADDR},
	{USDHC3_BASE_ADDR},
	{USDHC4_BASE_ADDR},
};

#define USDHC2_CD_GPIO	IMX_GPIO_NR(2, 2)
#define USDHC3_CD_GPIO	IMX_GPIO_NR(2, 0)

int board_mmc_getcd(struct mmc *mmc)
{
	struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;
	int ret = 0;

	switch (cfg->esdhc_base) {
	case USDHC2_BASE_ADDR:
		ret = !gpio_get_value(USDHC2_CD_GPIO);
		break;
	case USDHC3_BASE_ADDR:
		ret = !gpio_get_value(USDHC3_CD_GPIO);
		break;
	case USDHC4_BASE_ADDR:
		ret = 1; /* eMMC/uSDHC4 is always present */
		break;
	}

	return ret;
}

int board_mmc_init(bd_t *bis)
{
	int ret;

	imx_iomux_v3_setup_multiple_pads(
		usdhc2_pads, ARRAY_SIZE(usdhc2_pads));
	gpio_direction_input(USDHC2_CD_GPIO);
	usdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC2_CLK);
	ret = fsl_esdhc_initialize(bis, &usdhc_cfg[0]);
	if (ret)
		return ret;

	imx_iomux_v3_setup_multiple_pads(
		usdhc3_pads, ARRAY_SIZE(usdhc3_pads));
	gpio_direction_input(USDHC3_CD_GPIO);
	usdhc_cfg[1].sdhc_clk = mxc_get_clock(MXC_ESDHC3_CLK);
	ret = fsl_esdhc_initialize(bis, &usdhc_cfg[1]);
	if (ret)
		return ret;

	imx_iomux_v3_setup_multiple_pads(
		usdhc4_pads, ARRAY_SIZE(usdhc4_pads));
	usdhc_cfg[2].sdhc_clk = mxc_get_clock(MXC_ESDHC4_CLK);
	ret = fsl_esdhc_initialize(bis, &usdhc_cfg[2]);
	if (ret)
		return ret;

	return 0;
}
#endif

int mx6_rgmii_rework(struct phy_device *phydev)
{
	unsigned short val;

	/* To enable AR8031 ouput a 125MHz clk from CLK_25M */
	phy_write(phydev, MDIO_DEVAD_NONE, 0xd, 0x7);
	phy_write(phydev, MDIO_DEVAD_NONE, 0xe, 0x8016);
	phy_write(phydev, MDIO_DEVAD_NONE, 0xd, 0x4007);

	val = phy_read(phydev, MDIO_DEVAD_NONE, 0xe);
	val &= 0xffe3;
	val |= 0x18;
	phy_write(phydev, MDIO_DEVAD_NONE, 0xe, val);

	/* introduce tx clock delay */
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x5);
	val = phy_read(phydev, MDIO_DEVAD_NONE, 0x1e);
	val |= 0x0100;
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, val);

	return 0;
}

int board_phy_config(struct phy_device *phydev)
{
	mx6_rgmii_rework(phydev);

	if (phydev->drv->config)
		phydev->drv->config(phydev);

	return 0;
}

#if defined(CONFIG_VIDEO_IPUV3)
static void disable_lvds(struct display_info_t const *dev)
{
	struct iomuxc *iomux = (struct iomuxc *)IOMUXC_BASE_ADDR;

	int reg = readl(&iomux->gpr[2]);

	reg &= ~(IOMUXC_GPR2_LVDS_CH0_MODE_MASK |
		 IOMUXC_GPR2_LVDS_CH1_MODE_MASK);

	writel(reg, &iomux->gpr[2]);
}

static void enable_lvds(struct display_info_t const *dev)
{
	struct iomuxc *iomux = (struct iomuxc *)
				IOMUXC_BASE_ADDR;
	u32 reg = readl(&iomux->gpr[2]);
	reg |= IOMUXC_GPR2_DATA_WIDTH_CH0_24BIT;
	writel(reg, &iomux->gpr[2]);
}

static void enable_dual_lvds(struct display_info_t const *dev)
{
	struct iomuxc *iomux = (struct iomuxc *)
				IOMUXC_BASE_ADDR;
	u32 reg = readl(&iomux->gpr[2]);
	reg |= IOMUXC_GPR2_DATA_WIDTH_CH0_24BIT |
			IOMUXC_GPR2_DATA_WIDTH_CH1_24BIT |
			IOMUXC_GPR2_SPLIT_MODE_EN_MASK;
	writel(reg, &iomux->gpr[2]);
}

struct display_info_t const displays[] = {{
	.bus	= -1,
	.addr	= 0,
	.pixfmt	= IPU_PIX_FMT_RGB24,
	.detect	= NULL,
	.enable	= enable_lvds,
	.mode	= {
		.name           = "innolux_10_1",
		.refresh        = 60,
		.xres           = 1280,
		.yres           = 800,
		.pixclock       = 14065,
		.left_margin    = 40,
		.right_margin   = 40,
		.upper_margin   = 10,
		.lower_margin   = 3,
		.hsync_len      = 80,
		.vsync_len      = 10,
		.sync           = FB_SYNC_EXT,
		.vmode          = FB_VMODE_NONINTERLACED
} }, {
	.bus	= -1,
	.addr	= 0,
	.pixfmt	= IPU_PIX_FMT_RGB24,
	.detect	= NULL,
	.enable	= enable_lvds,
	.mode	= {
		.name           = "nec_12_1",
		.refresh        = 60,
		.xres           = 1280,
		.yres           = 800,
		.pixclock       = 14085,
		.left_margin    = 150,
		.right_margin   = 10,
		.upper_margin   = 20,
		.lower_margin   = 3,
		.hsync_len      = 60,
		.vsync_len      = 10,
		.sync           = FB_SYNC_EXT,
		.vmode          = FB_VMODE_NONINTERLACED
} }, {
	.bus	= -1,
	.addr	= 0,
	.pixfmt	= IPU_PIX_FMT_RGB24,
	.detect	= NULL,
	.enable	= enable_dual_lvds,
	.mode	= {
		.name           = "auo_18_5",
		.refresh        = 60,
		.xres           = 1920,
		.yres           = 1080,
		.pixclock       = 14085, //28130,
		.left_margin    = 60,
		.right_margin   = 44,
		.upper_margin   = 10,
		.lower_margin   = 5,
		.hsync_len      = 24,
		.vsync_len      = 5,
		.sync           = FB_SYNC_EXT,
		.vmode          = FB_VMODE_NONINTERLACED
} } };
size_t display_count = ARRAY_SIZE(displays);

static void setup_display(void)
{
	struct mxc_ccm_reg *mxc_ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;
	struct iomuxc *iomux = (struct iomuxc *)IOMUXC_BASE_ADDR;
	int reg;

	/* Setup HSYNC, VSYNC, DISP_CLK for debugging purposes */
	imx_iomux_v3_setup_multiple_pads(di0_pads, ARRAY_SIZE(di0_pads));

	enable_ipu_clock();

	/* Turn on LDB0, LDB1, IPU,IPU DI0 clocks */
	reg = readl(&mxc_ccm->CCGR3);
	reg |=  MXC_CCM_CCGR3_LDB_DI0_MASK | MXC_CCM_CCGR3_LDB_DI1_MASK;
	writel(reg, &mxc_ccm->CCGR3);

	/* set LDB0, LDB1 clk select to 011/011 */
	reg = readl(&mxc_ccm->cs2cdr);
	reg &= ~(MXC_CCM_CS2CDR_LDB_DI0_CLK_SEL_MASK
		 | MXC_CCM_CS2CDR_LDB_DI1_CLK_SEL_MASK);
	reg |= (3 << MXC_CCM_CS2CDR_LDB_DI0_CLK_SEL_OFFSET)
	      | (3 << MXC_CCM_CS2CDR_LDB_DI1_CLK_SEL_OFFSET);
	writel(reg, &mxc_ccm->cs2cdr);

	reg = readl(&mxc_ccm->cscmr2);
	reg |= MXC_CCM_CSCMR2_LDB_DI0_IPU_DIV | MXC_CCM_CSCMR2_LDB_DI1_IPU_DIV;
	writel(reg, &mxc_ccm->cscmr2);

	reg = readl(&mxc_ccm->chsccdr);
	reg |= (CHSCCDR_CLK_SEL_LDB_DI0
		<< MXC_CCM_CHSCCDR_IPU1_DI0_CLK_SEL_OFFSET);
	reg |= (CHSCCDR_CLK_SEL_LDB_DI0
		<< MXC_CCM_CHSCCDR_IPU1_DI1_CLK_SEL_OFFSET);
	writel(reg, &mxc_ccm->chsccdr);

	reg = IOMUXC_GPR2_BGREF_RRMODE_EXTERNAL_RES
	     | IOMUXC_GPR2_DI1_VS_POLARITY_ACTIVE_LOW
	     | IOMUXC_GPR2_DI0_VS_POLARITY_ACTIVE_LOW
	     | IOMUXC_GPR2_BIT_MAPPING_CH1_SPWG
	     | IOMUXC_GPR2_DATA_WIDTH_CH1_24BIT
	     | IOMUXC_GPR2_BIT_MAPPING_CH0_SPWG
	     | IOMUXC_GPR2_DATA_WIDTH_CH0_24BIT
	     | IOMUXC_GPR2_LVDS_CH0_MODE_ENABLED_DI0
	     | IOMUXC_GPR2_LVDS_CH1_MODE_ENABLED_DI0;
	writel(reg, &iomux->gpr[2]);

	clrsetbits_le32(&iomux->gpr[3],
		IOMUXC_GPR3_LVDS0_MUX_CTL_MASK |
		IOMUXC_GPR3_LVDS1_MUX_CTL_MASK,
		(IOMUXC_GPR3_MUX_SRC_IPU1_DI0 <<
		 IOMUXC_GPR3_LVDS0_MUX_CTL_OFFSET) |
		(IOMUXC_GPR3_MUX_SRC_IPU1_DI0 <<
		 IOMUXC_GPR3_LVDS1_MUX_CTL_OFFSET)
	);
}
#endif /* CONFIG_VIDEO_IPUV3 */

/*
 * Do not overwrite the console
 * Use always serial for U-Boot console
 */
int overwrite_console(void)
{
	return 1;
}

int board_eth_init(bd_t *bis)
{
	setup_iomux_enet();
	//setup_pcie();

	return cpu_eth_init(bis);
}

#ifdef CONFIG_USB_EHCI_MX6
#define USB_OTHERREGS_OFFSET	0x800
#define UCTRL_PWR_POL		(1 << 9)

static iomux_v3_cfg_t const usb_otg_pads[] = {
	MX6_PAD_EIM_D22__USB_OTG_PWR | MUX_PAD_CTRL(NO_PAD_CTRL),
	MX6_PAD_ENET_RX_ER__USB_OTG_ID | MUX_PAD_CTRL(NO_PAD_CTRL),
};

static iomux_v3_cfg_t const usb_hc1_pads[] = {
	MX6_PAD_ENET_TXD1__GPIO1_IO29 | MUX_PAD_CTRL(NO_PAD_CTRL),
};

static void setup_usb(void)
{
	imx_iomux_v3_setup_multiple_pads(usb_otg_pads,
					 ARRAY_SIZE(usb_otg_pads));

	/*
	 * set daisy chain for otg_pin_id on 6q.
	 * for 6dl, this bit is reserved
	 */
	imx_iomux_set_gpr_register(1, 13, 1, 0);

	imx_iomux_v3_setup_multiple_pads(usb_hc1_pads,
					 ARRAY_SIZE(usb_hc1_pads));
}

int board_ehci_hcd_init(int port)
{
	u32 *usbnc_usb_ctrl;

	if (port > 1)
		return -EINVAL;

	usbnc_usb_ctrl = (u32 *)(USB_BASE_ADDR + USB_OTHERREGS_OFFSET +
				 port * 4);

	setbits_le32(usbnc_usb_ctrl, UCTRL_PWR_POL);

	return 0;
}

int board_ehci_power(int port, int on)
{
	switch (port) {
	case 0:
		break;
	case 1:
		if (on)
			gpio_direction_output(IMX_GPIO_NR(1, 29), 1);
		else
			gpio_direction_output(IMX_GPIO_NR(1, 29), 0);
		break;
	default:
		printf("MXC USB port %d not yet supported\n", port);
		return -EINVAL;
	}

	return 0;
}
#endif

int board_early_init_f(void)
{
	/* Setup UARTs */
	setup_pcie();
	setup_iomux_uart();
#if defined(CONFIG_VIDEO_IPUV3)
	setup_display();
#endif

	return 0;
}

int board_init(void)
{
	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_SDRAM + 0x100;

#ifdef CONFIG_MXC_SPI
	setup_spi();
#endif
	setup_i2c(1, CONFIG_SYS_I2C_SPEED, 0x7f, &i2c1_pad_info);
	setup_i2c(2, CONFIG_SYS_I2C_SPEED, 0x7f, &i2c2_pad_info);
	setup_i2c(3, CONFIG_SYS_I2C_SPEED, 0x7f, &i2c3_pad_info);

#ifdef CONFIG_USB_EHCI_MX6
	setup_usb();
#endif

#ifdef CONFIG_CMD_SATA
    setup_sata();
#endif

	return 0;
}

int power_init_board(void)
{
	struct pmic *p;
	unsigned int ret;

	p = pfuze_common_init(I2C_PMIC);
	if (!p)
		return -ENODEV;

	ret = pfuze_mode_init(p, APS_PFM);
	if (ret < 0)
		return ret;
#if 0
	/* Set VGEN1 to 1.2V */
	pmic_reg_read(p, PFUZE100_VGEN1VOL, &reg);
	reg &= ~LDO_VOL_MASK;
	reg |= LDOA_1_20V;
	pmic_reg_write(p, PFUZE100_VGEN1VOL, reg);

	/* Set VGEN2 to 1.5V */
	pmic_reg_read(p, PFUZE100_VGEN2VOL, &reg);
	reg &= ~LDO_VOL_MASK;
	reg |= LDOA_1_50V;
	pmic_reg_write(p, PFUZE100_VGEN2VOL, reg);

	/* Set VGEN3 to 1.8V */
	pmic_reg_read(p, PFUZE100_VGEN3VOL, &reg);
	reg &= ~LDO_VOL_MASK;
	reg |= LDOB_1_80V;
	pmic_reg_write(p, PFUZE100_VGEN3VOL, reg);

	/* Set VGEN4 to 1.8V */
	pmic_reg_read(p, PFUZE100_VGEN4VOL, &reg);
	reg &= ~LDO_VOL_MASK;
	reg |= LDOB_1_80V;
	pmic_reg_write(p, PFUZE100_VGEN4VOL, reg);

	/* Set VGEN5 to 2.5 */
	pmic_reg_read(p, PFUZE100_VGEN5VOL, &reg);
	reg &= ~LDO_VOL_MASK;
	reg |= LDOB_2_50V;
	pmic_reg_write(p, PFUZE100_VGEN5VOL, reg);

	/* Set VGEN6 to 2.8 */
	pmic_reg_read(p, PFUZE100_VGEN6VOL, &reg);
	reg &= ~LDO_VOL_MASK;
	reg |= LDOB_2_80V;
	pmic_reg_write(p, PFUZE100_VGEN6VOL, reg);
#endif
	return 0;
}

#ifdef CONFIG_MXC_SPI
int board_spi_cs_gpio(unsigned bus, unsigned cs)
{
	return (bus == 0 && cs == 0) ? (IMX_GPIO_NR(2, 30)) : -1;
}
#endif

#ifdef CONFIG_CMD_BMODE
static const struct boot_mode board_boot_modes[] = {
	/* 4 bit bus width */
	{"sd2",	 MAKE_CFGVAL(0x40, 0x28, 0x00, 0x00)},
	{"sd3",	 MAKE_CFGVAL(0x40, 0x30, 0x00, 0x00)},
	/* 8 bit bus width */
	{"emmc", MAKE_CFGVAL(0x40, 0x38, 0x00, 0x00)},
	{NULL,	 0},
};
#endif

int board_late_init(void)
{
	char mac_addr[6];

#ifdef CONFIG_CMD_BMODE
	add_board_boot_modes(board_boot_modes);
#endif

	/* Get the display type from the PIC */
	rdu2_pic_get_display_type();

	/* Tell the PIC that the display data from the MX6 is stable */
	rdu2_pic_send_display_data_stable();

	/* Get MAC address for eth0 and eth1 */
	get_mx6_board_mac_addr(0, mac_addr);
	get_mx6_board_mac_addr(1, mac_addr);

	/* Get the u-boot part number */
	get_zii_part_number();

	return 0;
}


/*
 * called prior to booting kernel or by 'fdt boardsetup' command
 *
 * unless 'fdt_noauto' env var is set we will update the following in the DTB:
 *  - display setup
 */
int ft_board_setup(void *blob, bd_t *bd)
{
	int off, toff;
	int display_index;
	char *ptr;

	const char *display = getenv("display");

	/* Update display timings from display env var */
	if (display) {
		printf("   Set display timings for %s...\n", display);

		display_index = simple_strtoul(display, &ptr, 10);
		if(displayData[display_index].interface_type == INTERFACE_TYPE__SINGLE_LVDS ||
				displayData[display_index].interface_type == INTERFACE_TYPE__DUAL_LVDS)
		{
			toff = fdt_path_offset(blob, "ldb");
			if (toff < 0)
				return toff;
			/* Set status to okay */
			fdt_setprop(blob, toff, "status", "okay", 5);
			if(displayData[display_index].interface_type == INTERFACE_TYPE__DUAL_LVDS)
			{
				fdt_setprop(blob, toff, "split-mode", 0, 0);
			}

			/* Get the next node down - lvds-channel@0 */
			toff = fdt_subnode_offset(blob, toff, "lvds-channel@0");
			if (toff < 0)
				return toff;
			/* Set status to okay */
			fdt_setprop(blob, toff, "status", "okay", 5);

			toff = fdt_subnode_offset(blob, toff, "display-timings");
			if (toff < 0)
					return toff;

			/* Get the next node down - display-timings */
			for (off = fdt_first_subnode(blob, toff); off >= 0; off = fdt_next_subnode(blob, off)) {
				uint32_t h = fdt_get_phandle(blob, off);
				if (strcasecmp(fdt_get_name(blob, off, NULL), displayData[display_index].display_name) == 0)
					return fdt_setprop_u32(blob, toff, "native-mode", h);
			}
		}
	}

	return 0;
}


int checkboard(void)
{
	puts("Board: Zii RDU2\n");
	return 0;
}

#ifdef CONFIG_SPL_BUILD
#include <spl.h>
#include <libfdt.h>
#include "dram_test.h"

const struct mx6dq_iomux_ddr_regs mx6_ddr_ioregs = {
/* ddr configs for RDU2 */
        .dram_sdclk_0   =  0x00000030, /*20e0588*/
        .dram_sdclk_1   =  0x00000030, /*20e0594*/
        .dram_cas       =  0x00000030, /*20e056c*/
        .dram_ras       =  0x00000030, /*20e0578*/
        .dram_reset     =  0x00000030, /*20e057c*/
        .dram_sdcke0    =  0x00003000, /*20e0590*/
        .dram_sdcke1    =  0x00003000, /*20e0598*/
        .dram_sdba2     =  0x00000000, /*20e058c*/
        .dram_sdodt0    =  0x00000030, /*20e059c*/
        .dram_sdodt1    =  0x00000030, /*20e05a0*/
        .dram_sdqs0     =  0x00000028, /*20e05a8*/
        .dram_sdqs1     =  0x00000028, /*20e05b0*/
        .dram_sdqs2     =  0x00000028, /*20e0524*/
        .dram_sdqs3     =  0x00000028, /*20e051c*/
        .dram_sdqs4     =  0x00000028, /*20e0518*/
        .dram_sdqs5     =  0x00000028, /*20e050c*/
        .dram_sdqs6     =  0x00000028, /*20e05b8*/
        .dram_sdqs7     =  0x00000028, /*20e05c0*/
        .dram_dqm0      =  0x00020028, /*20e05ac*/
        .dram_dqm1      =  0x00020028, /*20e054b*/
        .dram_dqm2      =  0x00020028, /*20e0528*/
        .dram_dqm3      =  0x00020028, /*20e0520*/
        .dram_dqm4      =  0x00020028, /*20e0514*/
        .dram_dqm5      =  0x00020028, /*20e0510*/
        .dram_dqm6      =  0x00020028, /*20e05bc*/
        .dram_dqm7      =  0x00020028, /*20e05c4*/
};

const struct mx6dq_iomux_grp_regs mx6_grp_ioregs = {
/* grp configs for RDU2 */
        .grp_ddr_type           =  0x000C0000, /*20e0798*/
        .grp_ddrmode_ctl        =  0x00020000, /*20e0750*/
        .grp_ddrpke             =  0x00000000, /*20e0758*/
        .grp_addds              =  0x00000030, /*20e074c*/
        .grp_ctlds              =  0x00000030, /*20e078c*/
        .grp_ddrmode            =  0x00020000, /*20e0774*/
        .grp_b0ds               =  0x00000028, /*20e0784*/
        .grp_b1ds               =  0x00000028, /*20e0788*/
        .grp_b2ds               =  0x00000028, /*20e0794*/
        .grp_b3ds               =  0x00000028, /*20e079c*/
        .grp_b4ds               =  0x00000028, /*20e07a0*/
        .grp_b5ds               =  0x00000028, /*20e07a4*/
        .grp_b6ds               =  0x00000028, /*20e07a8*/
        .grp_b7ds               =  0x00000028, /*20e0748*/
};

const struct mx6_mmdc_calibration mx6_mmcd_calib = {
/* ddr configs for RDU2 */
        .p0_mpwldectrl0 =  0x001F001F, /*21b080c*/
        .p0_mpwldectrl1 =  0x001F001F, /*21b0810*/
        .p1_mpwldectrl0 =  0x001F001F, /*21b480c*/
        .p1_mpwldectrl1 =  0x001F001F, /*21b4810*/
        .p0_mpdgctrl0 =  0x43260335, /*21b083c*/
        .p0_mpdgctrl1 =  0x031A030B, /*21b0840*/
        .p1_mpdgctrl0 =  0x4323033B, /*21b483c*/
        .p1_mpdgctrl1 =  0x0323026F, /*21b4840*/
        .p0_mprddlctl =  0x4436383B, /*21b0848*/
        .p1_mprddlctl =  0x39393341, /*21b4848*/
        .p0_mpwrdlctl =  0x35373933, /*21b0850*/
        .p1_mpwrdlctl =  0x48254A36, /*21b4850*/
};

static struct mx6_ddr3_cfg mem_ddr = {
/* RDU2 Samsung K4B4G16 */
        .mem_speed = 1600,
        .density = 4,
        .width = 16, 
        .banks = 8,
        .rowaddr = 15, 
        .coladdr = 10, 
        .pagesz = 2,
        .trcd = 1375,
        .trcmin = 4875,
        .trasmin = 3500,
};

static void ccgr_init(void)
{
	struct mxc_ccm_reg *ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;

	writel(0x00C03F3F, &ccm->CCGR0);
	writel(0x0030FC03, &ccm->CCGR1);
	writel(0x0FFFC000, &ccm->CCGR2);
	writel(0x3FF00000, &ccm->CCGR3);
	writel(0x00FFF300, &ccm->CCGR4);
	writel(0x0F0000C3, &ccm->CCGR5);
	writel(0x000003FF, &ccm->CCGR6);
}

static void gpr_init(void)
{
	struct iomuxc *iomux = (struct iomuxc *)IOMUXC_BASE_ADDR;

	/* enable AXI cache for VDOA/VPU/IPU */
	writel(0xF00000CF, &iomux->gpr[4]);
	/* set IPU AXI-id0 Qos=0xf(bypass) AXI-id1 Qos=0x7 */
	writel(0x007F007F, &iomux->gpr[6]);
	writel(0x007F007F, &iomux->gpr[7]);
}

/*
 * This section requires the differentiation between iMX6 Sabre boards, but
 * for now, it will configure only for the mx6q variant.
 */
static void spl_dram_init(void)
{
	struct mx6_ddr_sysinfo sysinfo = {
		/* width of data bus:0=16,1=32,2=64 */
		.dsize = 2,
		/* config for full 4GB range so that get_mem_size() works */
		.cs_density = 32, /* 32Gb per CS */
		/* single chip select */
		.ncs = 1,
		.cs1_mirror = 0,
		.rtt_wr = 1 /*DDR3_RTT_60_OHM*/,	/* RTT_Wr = RZQ/4 */
		.rtt_nom = 1 /*DDR3_RTT_60_OHM*/,	/* RTT_Nom = RZQ/4 */
		.walat = 1,	/* Write additional latency */
		.ralat = 5,	/* Read additional latency */
		.mif3_mode = 3,	/* Command prediction working mode */
		.bi_on = 1,	/* Bank interleaving enabled */
		.sde_to_rst = 0x10,	/* 14 cycles, 200us (JEDEC default) */
		.rst_to_cke = 0x23,	/* 33 cycles, 500us (JEDEC default) */
	};

	mx6dq_dram_iocfg(64, &mx6_ddr_ioregs, &mx6_grp_ioregs);
	mx6_dram_cfg(&sysinfo, &mx6_mmcd_calib, &mem_ddr);
}

void board_init_f(ulong dummy)
{

	/* setup AIPS and disable watchdog */
	arch_cpu_init();

	ccgr_init();
	gpr_init();

	/* iomux and setup of i2c */
	board_early_init_f();

	/* setup GP timer */
	timer_init();

	/* UART clocks enabled and gd valid - init serial console */
	preloader_console_init();

	/* DDR initialization */
	spl_dram_init();

        /* DDR test */
        t_dram_test();

	/* Clear the BSS. */
	memset(__bss_start, 0, __bss_end - __bss_start);

	/* load/boot image from boot device */
	board_init_r(NULL, 0);
}

void reset_cpu(ulong addr)
{
}
#endif
