/*
 * Copyright (C) 2015 Zodiac Inflight Innovations, Inc.
 *
 * Based on an original Makefile, which is:
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/iomux-vf610.h>
#include <asm/arch/ddrmc-vf610.h>
#include <asm/arch/crm_regs.h>
#include <asm/arch/clock.h>
#include <mmc.h>
#include <fsl_esdhc.h>
#include <miiphy.h>
#include <netdev.h>
#include <i2c.h>

DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL	(PAD_CTL_PUS_100K_UP | PAD_CTL_SPEED_MED | \
			PAD_CTL_DSE_25ohm | PAD_CTL_OBE_IBE_ENABLE)

#define ESDHC_PAD_CTRL	(PAD_CTL_PUS_100K_UP | PAD_CTL_SPEED_HIGH | \
			PAD_CTL_DSE_20ohm | PAD_CTL_OBE_IBE_ENABLE)

#define ENET_PAD_CTRL	(PAD_CTL_PUS_47K_UP | PAD_CTL_SPEED_HIGH | \
			PAD_CTL_DSE_50ohm | PAD_CTL_OBE_IBE_ENABLE)

#ifdef ZII_VYBRID_DEV_REV_A
   /*
    * Enable both supported sdhc0(eMMC) and sdhc1(SD)
    */
   #define CCM_CCGR7_SDHC_CTRL_MASK   ((CCM_CCGR7_SDHC1_CTRL_MASK) | (CCM_CCGR7_SDHC0_CTRL_MASK))
   #define CCM_CSCDR2_ESDHC_EN        ((CCM_CSCDR2_ESDHC1_EN) | (CCM_CSCDR2_ESDHC0_EN))
   #define CCM_CSCDR2_ESDHC_CLK_DIV   ((CCM_CSCDR2_ESDHC1_CLK_DIV(0)) | (CCM_CSCDR2_ESDHC0_CLK_DIV(0)))
   #define CCM_CSCMR1_ESDHC_CLK_SEL   ((CCM_CSCMR1_ESDHC1_CLK_SEL(3)) | (CCM_CSCMR1_ESDHC0_CLK_SEL(3)))
#else
   #define CCM_CCGR7_SDHC_CTRL_MASK   (CCM_CCGR7_SDHC1_CTRL_MASK)
   #define CCM_CSCDR2_ESDHC_EN        (CCM_CSCDR2_ESDHC1_EN)
   #define CCM_CSCDR2_ESDHC_CLK_DIV   CCM_CSCDR2_ESDHC1_CLK_DIV(0)
   #define CCM_CSCMR1_ESDHC_CLK_SEL   CCM_CSCMR1_ESDHC1_CLK_SEL(3)
#endif

int dram_init(void)
{
	struct ddrmc_lvl_info lvl = {
		.wrlvl_reg_en    = 1,
		.wrlvl_dl_0      = 12,
		.wrlvl_dl_1      = 0,
		.rdlvl_gt_reg_en = 0,
		.rdlvl_gt_dl_0   = 0,
		.rdlvl_gt_dl_1   = 12,
		.rdlvl_reg_en    = 1,
		.rdlvl_dl_0      = 0,
		.rdlvl_dl_1      = 0,
	};

	static const struct ddr3_jedec_timings timings = {
		.tinit           = 5,
		.trst_pwron      = 80000,
		.cke_inactive    = 200000,
		.wrlat           = 5,
		.caslat_lin      = 6,
		.trc             = 6,
		.trrd            = 4,
		.tccd            = 4,
		.tfaw            = 16,
		.trp             = 6,
		.twtr            = 4,
		.tras_min        = 14,
		.tmrd            = 4,
		.trtp            = 4,
		.tras_max        = 28080,
		.tmod            = 12,
		.tckesr          = 4,
		.tcke            = 3,
		.trcd_int        = 6,
		.tdal            = 12,
		.tdll            = 512,
		.trp_ab          = 6,
		.tref            = 3120,
		.trfc            = 104,
		.tpdex           = 3,
		.txpdll          = 10,
		.txsnr           = 108,
		.txsr            = 512,
		.cksrx           = 5,
		.cksre           = 5,
		.zqcl            = 256,
		.zqinit          = 512,
		.zqcs            = 64,
		.ref_per_zq      = 64,
		.aprebit         = 10,
		.wlmrd           = 40,
		.wldqsen         = 25,
	};

	ddrmc_setup_iomux();

	ddrmc_ctrl_init_ddr3(&timings, &lvl, 1, 1);
	gd->ram_size = get_ram_size((void *)PHYS_SDRAM, PHYS_SDRAM_SIZE);

	return 0;
}

static void setup_iomux_uart0(void)
{
	static const iomux_v3_cfg_t uart0_pads[] = {
		NEW_PAD_CTRL(VF610_PAD_PTB10__UART0_TX, UART_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTB11__UART0_RX, UART_PAD_CTRL),
	};

	imx_iomux_v3_setup_multiple_pads(uart0_pads, ARRAY_SIZE(uart0_pads));
}



static void setup_iomux_uart1(void)
{
	static const iomux_v3_cfg_t uart1_pads[] = {
    NEW_PAD_CTRL(VF610_PAD_PTB23__UART1_TX, UART_PAD_CTRL),
    NEW_PAD_CTRL(VF610_PAD_PTB24__UART1_RX, UART_PAD_CTRL),

#ifdef ZII_VYBRID_DEV_REV_A
    NEW_PAD_CTRL(VF610_PAD_PTB25__UART1_RTS, UART_PAD_CTRL),
#else
    NEW_PAD_CTRL(VF610_PAD_PTE28__GPIO_133,VF610_GPIO_PAD_CTRL)
#endif
	};
	imx_iomux_v3_setup_multiple_pads(uart1_pads, ARRAY_SIZE(uart1_pads));
}

static void setup_iomux_uart2(void)
{
#ifdef ZII_VYBRID_DEV_REV_A
	static const iomux_v3_cfg_t uart2_pads[] = {
		NEW_PAD_CTRL(VF610_PAD_PTD0__UART2_TX, UART_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTD1__UART2_RX, UART_PAD_CTRL),
	};
#else
	static const iomux_v3_cfg_t uart2_pads[] = {
		NEW_PAD_CTRL(VF610_PAD_PTD23__UART2_TX, UART_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTD22__UART2_RX, UART_PAD_CTRL),
	};
#endif

	imx_iomux_v3_setup_multiple_pads(uart2_pads, ARRAY_SIZE(uart2_pads));
}


#if 0
static void setup_iomux_enet(void)
{
	static const iomux_v3_cfg_t enet0_pads[] = {
		NEW_PAD_CTRL(VF610_PAD_PTA6__RMII0_CLKIN, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC1__RMII0_MDIO, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC0__RMII0_MDC, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC2__RMII0_CRS_DV, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC3__RMII0_RD1, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC4__RMII0_RD0, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC5__RMII0_RXER, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC6__RMII0_TD1, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC7__RMII0_TD0, ENET_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC8__RMII0_TXEN, ENET_PAD_CTRL),
	};

	imx_iomux_v3_setup_multiple_pads(enet0_pads, ARRAY_SIZE(enet0_pads));
}
#endif

static void setup_iomux_ADC(void)
{
	static const iomux_v3_cfg_t ADC_pads[] = {
		VF610_PAD_PTA18__ADC0_SE0,
		VF610_PAD_PTA19__ADC0_SE1,
		VF610_PAD_PTB0__ADC0_SE2,
		VF610_PAD_PTB2__ADC1_SE2,
		VF610_PAD_PTB3__ADC1_SE3,
		VF610_PAD_PTB4__ADC0_SE4,
		VF610_PAD_PTB5__ADC1_SE4,
	};

	imx_iomux_v3_setup_multiple_pads(ADC_pads, ARRAY_SIZE(ADC_pads));
}

static void setup_iomux_i2c0(void)
{
	static const iomux_v3_cfg_t i2c0_pads[] = {
		VF610_PAD_PTB14__I2C0_SCL,
		VF610_PAD_PTB15__I2C0_SDA,
	};

	imx_iomux_v3_setup_multiple_pads(i2c0_pads, ARRAY_SIZE(i2c0_pads));
}

static void setup_iomux_i2c1(void)
{
	static const iomux_v3_cfg_t i2c1_pads[] = {
		VF610_PAD_PTB16__I2C1_SCL,
		VF610_PAD_PTB17__I2C1_SDA,
	};

	imx_iomux_v3_setup_multiple_pads(i2c1_pads, ARRAY_SIZE(i2c1_pads));
}

static void setup_iomux_i2c2(void)
{
	static const iomux_v3_cfg_t i2c2_pads[] = {
		VF610_PAD_PTA22__I2C2_SCL,
		VF610_PAD_PTA23__I2C2_SDA,
	};
	imx_iomux_v3_setup_multiple_pads(i2c2_pads, ARRAY_SIZE(i2c2_pads));
}

static void setup_iomux_qspi(void)
{
	static const iomux_v3_cfg_t qspi0_pads[] = {
#ifndef ZII_VYBRID_DEV_REV_A
		VF610_PAD_PTD0__QSPI0_A_QSCK,
		VF610_PAD_PTD1__QSPI0_A_CS0,
		VF610_PAD_PTD2__QSPI0_A_DATA3,
		VF610_PAD_PTD3__QSPI0_A_DATA2,
		VF610_PAD_PTD4__QSPI0_A_DATA1,
		VF610_PAD_PTD5__QSPI0_A_DATA0,
#endif
		VF610_PAD_PTD7__QSPI0_B_QSCK,
		VF610_PAD_PTD8__QSPI0_B_CS0,
		VF610_PAD_PTD9__QSPI0_B_DATA3,
		VF610_PAD_PTD10__QSPI0_B_DATA2,
		VF610_PAD_PTD11__QSPI0_B_DATA1,
		VF610_PAD_PTD12__QSPI0_B_DATA0,
	};

	imx_iomux_v3_setup_multiple_pads(qspi0_pads, ARRAY_SIZE(qspi0_pads));
}

static void setup_iomux_dspi0(void)
{
	static const iomux_v3_cfg_t dspi0_pads[] = {
		VF610_PAD_PTB18__DSPI0_CS1,
		VF610_PAD_PTB19__DSPI0_CS0,
		VF610_PAD_PTB20__DSPI0_SIN,
		VF610_PAD_PTB21__DSPI0_SOUT,
		VF610_PAD_PTB22__DSPI0_SCK,
	};

	imx_iomux_v3_setup_multiple_pads(dspi0_pads, ARRAY_SIZE(dspi0_pads));
}

static void setup_iomux_dspi2(void)
{
	static const iomux_v3_cfg_t dspi2_pads[] = {
		VF610_PAD_PTD31__DSPI2_CS1,
		VF610_PAD_PTD30__DSPI2_CS0,
		VF610_PAD_PTD29__DSPI2_SIN,
		VF610_PAD_PTD28__DSPI2_SOUT,
		VF610_PAD_PTD27__DSPI2_SCK,

	};

	imx_iomux_v3_setup_multiple_pads(dspi2_pads, ARRAY_SIZE(dspi2_pads));
}

static void setup_iomux_Arinc429Gpios(void)
{
	static const iomux_v3_cfg_t arinc429Gpios_pads[] = {
		VF610_PAD_PTB18__GPIO_40,
		VF610_PAD_PTB19__GPIO_41,
		VF610_PAD_PTB20__GPIO_42,
		VF610_PAD_PTB21__GPIO_43,
		VF610_PAD_PTB22__GPIO_44,
		VF610_PAD_PTB27__GPIO_97,
		VF610_PAD_PTB28__GPIO_98,

	};

	imx_iomux_v3_setup_multiple_pads(arinc429Gpios_pads, ARRAY_SIZE(arinc429Gpios_pads));
}

//#ifdef CONFIG_FSL_ESDHC
struct fsl_esdhc_cfg esdhc_cfg[2] = {
	{ESDHC0_BASE_ADDR},
	{ESDHC1_BASE_ADDR},
};

int board_mmc_getcd(struct mmc *mmc)
{
	struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;
	int ret = 0;

	switch (cfg->esdhc_base) {
#ifdef ZII_VYBRID_DEV_REV_A
	case ESDHC0_BASE_ADDR:
		ret = 1; /* eSDHC0 is always present */
		break;
#endif
	case ESDHC1_BASE_ADDR:
		//ret = !gpio_get_value(134);
		ret = 1;
		break;
	}

	return ret;
}

int board_mmc_init(bd_t *bis)
{
#ifdef ZII_VYBRID_DEV_REV_A
	static const iomux_v3_cfg_t esdhc0_pads[] = {
		NEW_PAD_CTRL(VF610_PAD_PTC0__ESDHC0_CLK,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC1__ESDHC0_CMD,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC2__ESDHC0_DAT0,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC3__ESDHC0_DAT1,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC4__ESDHC0_DAT2,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTC5__ESDHC0_DAT3,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTD23__ESDHC0_DAT4,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTD22__ESDHC0_DAT5,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTD21__ESDHC0_DAT6,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTD20__ESDHC0_DAT7,	ESDHC_PAD_CTRL),
	};
#endif

	static const iomux_v3_cfg_t esdhc1_pads[] = {
		NEW_PAD_CTRL(VF610_PAD_PTA24__ESDHC1_CLK,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTA25__ESDHC1_CMD,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTA26__ESDHC1_DAT0,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTA27__ESDHC1_DAT1,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTA28__ESDHC1_DAT2,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTA29__ESDHC1_DAT3,	ESDHC_PAD_CTRL),
		NEW_PAD_CTRL(VF610_PAD_PTA7__GPIO_134,		VF610_GPIO_PAD_CTRL),
	};

	esdhc_cfg[1].sdhc_clk = mxc_get_clock(MXC_ESDHC_CLK);
#ifdef ZII_VYBRID_DEV_REV_A
	esdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC_CLK);
#endif

#ifdef ZII_VYBRID_DEV_REV_A
	imx_iomux_v3_setup_multiple_pads(
		esdhc0_pads, ARRAY_SIZE(esdhc0_pads));
#endif

	imx_iomux_v3_setup_multiple_pads(
		esdhc1_pads, ARRAY_SIZE(esdhc1_pads));

#ifdef ZII_VYBRID_DEV_REV_A
	fsl_esdhc_initialize(bis, &esdhc_cfg[0]);
#endif

	return fsl_esdhc_initialize(bis, &esdhc_cfg[1]);
}

static void clock_init(void)
{
	struct ccm_reg *ccm = (struct ccm_reg *)CCM_BASE_ADDR;
	struct anadig_reg *anadig = (struct anadig_reg *)ANADIG_BASE_ADDR;

	clrsetbits_le32(&ccm->ccgr0, CCM_REG_CTRL_MASK,
			CCM_CCGR0_UART0_CTRL_MASK | 
			CCM_CCGR0_UART1_CTRL_MASK | 
			CCM_CCGR0_UART2_CTRL_MASK);
	//clrsetbits_le32(&ccm->ccgr0, CCM_REG_CTRL_MASK,
	//	CCM_CCGR0_UART1_CTRL_MASK);

#ifdef CONFIG_FSL_DSPI
	setbits_le32(&ccm->ccgr0, CCM_CCGR0_DSPI0_CTRL_MASK);
	setbits_le32(&ccm->ccgr0, CCM_CCGR0_DSPI1_CTRL_MASK);
	setbits_le32(&ccm->ccgr6, CCM_CCGR6_DSPI2_CTRL_MASK);
#endif

	clrsetbits_le32(&ccm->ccgr1, CCM_REG_CTRL_MASK,
		CCM_CCGR1_PIT_CTRL_MASK | CCM_CCGR1_ADC0_CTRL_MASK |
		CCM_CCGR1_USBC0_CTRL_MASK | CCM_CCGR1_WDOGA5_CTRL_MASK);
	clrsetbits_le32(&ccm->ccgr2, CCM_REG_CTRL_MASK,
		CCM_CCGR2_IOMUXC_CTRL_MASK | CCM_CCGR2_PORTA_CTRL_MASK |
		CCM_CCGR2_PORTB_CTRL_MASK | CCM_CCGR2_PORTC_CTRL_MASK |
		CCM_CCGR2_PORTD_CTRL_MASK | CCM_CCGR2_PORTE_CTRL_MASK |
		CCM_CCGR2_QSPI0_CTRL_MASK);
	clrsetbits_le32(&ccm->ccgr3, CCM_REG_CTRL_MASK,
		CCM_CCGR3_ANADIG_CTRL_MASK | CCM_CCGR3_SCSC_CTRL_MASK);
	clrsetbits_le32(&ccm->ccgr4, CCM_REG_CTRL_MASK,
		CCM_CCGR4_WKUP_CTRL_MASK | CCM_CCGR4_CCM_CTRL_MASK |
		CCM_CCGR4_GPC_CTRL_MASK | CCM_CCGR4_I2C0_CTRL_MASK | CCM_CCGR4_I2C1_CTRL_MASK);
	clrsetbits_le32(&ccm->ccgr6, CCM_REG_CTRL_MASK,
		CCM_CCGR6_OCOTP_CTRL_MASK | CCM_CCGR6_DDRMC_CTRL_MASK |
		CCM_CCGR6_DSPI2_CTRL_MASK);

	clrsetbits_le32(&ccm->ccgr7, CCM_REG_CTRL_MASK,
		CCM_CCGR7_SDHC_CTRL_MASK | CCM_CCGR7_ADC1_CTRL_MASK);
	clrsetbits_le32(&ccm->ccgr9, CCM_REG_CTRL_MASK,
		CCM_CCGR9_FEC0_CTRL_MASK | CCM_CCGR9_FEC1_CTRL_MASK);
	clrsetbits_le32(&ccm->ccgr10, CCM_REG_CTRL_MASK,
		CCM_CCGR10_NFC_CTRL_MASK | CCM_CCGR10_I2C2_CTRL_MASK);

	clrsetbits_le32(&anadig->pll2_ctrl, ANADIG_PLL2_CTRL_POWERDOWN,
		ANADIG_PLL2_CTRL_ENABLE | ANADIG_PLL2_CTRL_DIV_SELECT);
	clrsetbits_le32(&anadig->pll1_ctrl, ANADIG_PLL1_CTRL_POWERDOWN,
		ANADIG_PLL1_CTRL_ENABLE | ANADIG_PLL1_CTRL_DIV_SELECT);

	clrsetbits_le32(&ccm->ccr, CCM_CCR_OSCNT_MASK,
		CCM_CCR_FIRC_EN | CCM_CCR_OSCNT(5));
	clrsetbits_le32(&ccm->ccsr, CCM_REG_CTRL_MASK,
		CCM_CCSR_PLL1_PFD_CLK_SEL(3) | CCM_CCSR_PLL2_PFD4_EN |
		CCM_CCSR_PLL2_PFD3_EN | CCM_CCSR_PLL2_PFD2_EN |
		CCM_CCSR_PLL2_PFD1_EN | CCM_CCSR_PLL1_PFD4_EN |
		CCM_CCSR_PLL1_PFD3_EN | CCM_CCSR_PLL1_PFD2_EN |
		CCM_CCSR_PLL1_PFD1_EN | CCM_CCSR_DDRC_CLK_SEL(1) |
		CCM_CCSR_FAST_CLK_SEL(1) | CCM_CCSR_SYS_CLK_SEL(4));
	clrsetbits_le32(&ccm->cacrr, CCM_REG_CTRL_MASK,
		CCM_CACRR_IPG_CLK_DIV(1) | CCM_CACRR_BUS_CLK_DIV(2) |
		CCM_CACRR_ARM_CLK_DIV(0));
	clrsetbits_le32(&ccm->cscmr1, CCM_REG_CTRL_MASK,
		CCM_CSCMR1_ESDHC_CLK_SEL | CCM_CSCMR1_QSPI0_CLK_SEL(3) |
		CCM_CSCMR1_NFC_CLK_SEL(0));
	clrsetbits_le32(&ccm->cscdr1, CCM_REG_CTRL_MASK,
		CCM_CSCDR1_RMII_CLK_EN);
	clrsetbits_le32(&ccm->cscdr2, CCM_REG_CTRL_MASK,
		CCM_CSCDR2_ESDHC_EN | CCM_CSCDR2_ESDHC_CLK_DIV |
		CCM_CSCDR2_NFC_EN);
	clrsetbits_le32(&ccm->cscdr3, CCM_REG_CTRL_MASK,
		CCM_CSCDR3_QSPI0_EN | CCM_CSCDR3_QSPI0_DIV(1) |
		CCM_CSCDR3_QSPI0_X2_DIV(1) | CCM_CSCDR3_QSPI0_X4_DIV(3) |
		CCM_CSCDR3_NFC_PRE_DIV(5));
	clrsetbits_le32(&ccm->cscmr2, CCM_REG_CTRL_MASK,
		CCM_CSCMR2_RMII_CLK_SEL(0));

#ifdef CONFIG_USB_EHCI
        setbits_le32(&ccm->ccgr7, CCM_CCGR7_USBC1_CTRL_MASK);
#endif
}

static void mscm_init(void)
{
	struct mscm_ir *mscmir = (struct mscm_ir *)MSCM_IR_BASE_ADDR;
	int i;

	for (i = 0; i < MSCM_IRSPRC_NUM; i++)
		writew(MSCM_IRSPRC_CP0_EN, &mscmir->irsprc[i]);
}

int board_phy_config(struct phy_device *phydev)
{
	if (phydev->drv->config)
		phydev->drv->config(phydev);

	return 0;
}

int board_early_init_f(void)
{
	clock_init();
	mscm_init();

	setup_iomux_uart0();
	setup_iomux_uart1();
	setup_iomux_uart2();
	setup_iomux_i2c0();
	setup_iomux_i2c1();
	setup_iomux_i2c2();
	setup_iomux_qspi();
	setup_iomux_dspi0();
	setup_iomux_dspi2();
	setup_iomux_Arinc429Gpios();
	setup_iomux_ADC();

	return 0;
}

int board_init(void)
{
	struct scsc_reg *scsc = (struct scsc_reg *)SCSC_BASE_ADDR;

	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_SDRAM + 0x100;

	/*
	 * Enable external 32K Oscillator
	 *
	 * The internal clock experiences significant drift
	 * so we must use the external oscillator in order
	 * to maintain correct time in the hwclock
	 */
	setbits_le32(&scsc->sosc_ctr, SCSC_SOSC_CTR_SOSC_EN);

	return 0;
}

int checkboard(void)
{
	puts("Board: Zii Vybrid Dev\n");

	return 0;
}

