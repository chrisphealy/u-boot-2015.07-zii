
/* #define DEBUG */

#include <common.h>
#include <environment.h>


#define JTAG_ENV_KEY "Zii UBoot Environment override via JTAG. Version 1.00.00"
#define JTAG_ENV_HEADER_LENGTH 64

#ifdef CONFIG_ENV_JTAG_USE_BUILT_IN_VERSION

static struct {
	char Header[JTAG_ENV_HEADER_LENGTH];
	char Crc[4];
	char Env[CONFIG_ENV_SIZE];
} env_jtag_built_in_version = {
		JTAG_ENV_KEY,
		"CRC",
		{
				"baudrate=115200\0"
				"bootargs_kraken=setenv bootargs console=${console},${baudrate} root=/dev/nfs r nfsroot=${serverip}:${rootpath} ip=${ipaddr}:${serverip}:${serverip}:${netmask}:RAMSTETTERCFU1::eth1:off\0"
				"bootcmd=run bootcmd_kraken\0"
				"bootcmd_kraken=run bootargs_kraken; tftpboot ${loadaddr} uImage; tftpboot ${fdt_addr} dtb.dtb; bootm ${loadaddr} - ${fdt_addr}\0"
				"bootdelay=1\0"
				"console=ttymxc0\0"
				"ethact=FEC\0"
				"ethaddr=1e:e7:de:ad:be:ef\0"
				"ethprime=FEC\0"
				"fdt_addr=0x18000000\0"
				"fdt_high=0xffffffff\0"
				"ipaddr=172.16.0.173\0"
				"loadaddr=0x12000000\0"
				"netmask=255.255.0.0\0"
				"rootpath=/opt/workspace/srv/nfs/cfu1\0"
				"serverip=172.16.0.1\0"
				"\000\000\000\000"
		}
};


void env_jtag_set_built_in_version(void)
{
	void* env_header_offset = (void *)CONFIG_ENV_JTAG_OFFSET;

	char * buf = (char *)env_jtag_built_in_version.Env;
	int i;
	int zeroCnt = 0;

	printf("Setting Rick's JTAG environment as per common/env_jtag.c\n");

	for (i=0; i < CONFIG_ENV_SIZE; ++i) {
		if (0 == buf[i]) {
			++zeroCnt;
			if (2 == zeroCnt) {
				break;
			}

		} else {
			zeroCnt = 0;
		}
	}
	for (; i < sizeof(env_jtag_built_in_version); ++i) {
		buf[i] = 0;
	}
	memcpy(env_header_offset, &env_jtag_built_in_version, sizeof(env_jtag_built_in_version));
}

#endif




int env_relocate_jtag(void)
{
	static char jtag_env_detect_key[] = JTAG_ENV_KEY;
	/* Calculate the size of the key ignoring the zero term which will not be present */
	static u32  key_length = sizeof(jtag_env_detect_key) - sizeof(jtag_env_detect_key[0]);
	void* env_header_offset = (void *)CONFIG_ENV_JTAG_OFFSET;
	void* env_offset = (void *)(CONFIG_ENV_JTAG_OFFSET + JTAG_ENV_HEADER_LENGTH);

#ifdef CONFIG_ENV_JTAG_USE_BUILT_IN_VERSION
	env_jtag_set_built_in_version();
#endif
	if (0 != memcmp( env_header_offset, jtag_env_detect_key, key_length)) {
		printf("JTAGed environment not found at 0x%08X\n", CONFIG_ENV_JTAG_OFFSET);
		return 0;
	}

	if (1 != env_import(env_offset, 0)) {
		printf( "JTAGed environment import failed\n");
		return 0;
	}

	printf( "Using JTAGed environment\n");
	return 1;
}


