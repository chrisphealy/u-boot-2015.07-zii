//*****************************************************************************
// Disclosure:
// Copyright(C) 2010 Systems and Software Enterprises, Inc. (dba IMS)
// ALL RIGHTS RESERVED.
// The contents of this medium may not be reproduced in whole or part without
// the written consent of IMS, except as authorized by section 117 of the U.S.
// Copyright law.
//*****************************************************************************

#include <dm.h>
#include <dm/lists.h>
#include <dm/device-internal.h>
#include <common.h>
#include <asm/errno.h>
#include <serial.h>

#include "zii_pic_comm_base.h"

DECLARE_GLOBAL_DATA_PTR;

#define SOH 0x01
#define STX 0x02
#define ETX 0x03
#define EOT 0x04
#define ACK 0x06
#define BSP 0x08
#define DLE 0x10
#define NAK 0x15
#define CAN 0x18
#define EOF 0x1A

// Local Function Prototypes
int 		pic_escape_data(unsigned char *out, const unsigned char* in, int len);
uint8_t 	pic_calc_checksum(const unsigned char * in, int len);
uint16_t 	UpdateCrc16_CCITT(uint16_t prevValue, uint8_t nextByte);
uint16_t 	CalculateCrc16_CCITT(const uint8_t * buf, uint32_t len);
int 		pic_pack_msg(unsigned char *out, unsigned char *in, int len, msgErrorDetectionType_E detectType);

void pic_reset_comms(struct udevice *dev)
{
	struct dm_serial_ops *ops = serial_get_ops(dev);

	ops->putc(dev, '\x03');
	ops->putc(dev, '\x03');
	ops->putc(dev, '\x03');
}


void pic_uart_init(struct udevice *dev, int baudRate)
{
	struct dm_serial_ops *ops = serial_get_ops(dev);

	int ret = ops->setbrg(dev, baudRate);
	if(ret != 0) {
		printf("Unable to set baud rate\n");
	}

    pic_reset_comms(dev);
}



int pic_escape_data(unsigned char *framedBuf, const unsigned char* unframedBuf, int len)
{
	int x               = 0;
    unsigned char temp  = 0;
    int framedIndex     = 0;

    // Add the start of the frame
    framedBuf[framedIndex++] = STX;
	for(x = 0; x < len; x++)
	{
		temp = unframedBuf[x];
		if (temp == ETX || temp == DLE || temp == STX) {
		    framedBuf[framedIndex++] = DLE;
		}
		framedBuf[framedIndex++] = temp;
	}

	framedBuf[framedIndex++] = ETX;

	return framedIndex;
}


// Calculates the simple checksum, and returns
unsigned char pic_calc_checksum(const unsigned char * in, int len)
{
	unsigned char sum = 0;
	while(len--)
		sum += *in++;
	return 0x100 - sum;
}


uint16_t UpdateCrc16_CCITT(uint16_t prevValue, uint8_t nextByte)
{
    unsigned crc_new = (uint8_t)(prevValue >> 8) | (prevValue << 8);
    crc_new ^= nextByte;
    crc_new ^= (uint8_t)(crc_new & 0xff) >> 4;
    crc_new ^= crc_new << 12;
    crc_new ^= (crc_new & 0xff) << 5;

    return crc_new;
}


// Calculates CCITT CRC16 of the buffer and returns
uint16_t CalculateCrc16_CCITT(const uint8_t * buf, uint32_t len)
{
	uint32_t x = 0;
	//Initialize the crc value
	uint16_t crc = 0xFFFF;

	for(x = 0; x < len; x++)
	{
		crc = UpdateCrc16_CCITT(crc, buf[x]);
	}
	return ((crc >> 8) | ((crc << 8) & 0xFF00)) ;
}


int pic_ack_id = 0;
// Packs message into pic packet
// Returns total message length
int pic_pack_msg(unsigned char *framedMsg, unsigned char *unframedMsg, int len, msgErrorDetectionType_E detectType)
{
	unsigned char checksum	= 0;
	uint16_t      crc16 	= 0;
	int unframedMsgLen      = len;
	int framedMsgLen        = 0;

	// Pack ack, data, and checksum into temp structure,
	// because we have to escape the whole thing
	unframedMsg[1] = ++pic_ack_id;

	switch(detectType)
	{
	case msgErrorDetectionType_Checksum:
		// Calculate the checksum
		checksum = pic_calc_checksum(unframedMsg, unframedMsgLen);

		// Store the checksum
		unframedMsg[unframedMsgLen++] = checksum;
		break;
	case msgErrorDetectionType_Crc:
		// Calculate CRC
		crc16 = CalculateCrc16_CCITT(unframedMsg, unframedMsgLen);

		// Store CRC
		unframedMsg[unframedMsgLen++] = (uint8_t)(crc16 & 0x00FF);
		unframedMsg[unframedMsgLen++] = (uint8_t)(crc16 >> 8);
		break;
	}

	framedMsgLen = pic_escape_data(framedMsg, unframedMsg, unframedMsgLen);

	return framedMsgLen;
}


// Sends the created message to the PIC
void pic_send_msg(struct udevice *dev, uint8_t *unframedMsg, int unframedMsgLen, msgErrorDetectionType_E detectType) {
	struct dm_serial_ops *ops = serial_get_ops(dev);
	uint8_t framedMsg[256]	= {0};
	int fullMessageLen 	    = 0;
	int x                   = 0;
	int err = 0;

	fullMessageLen = pic_pack_msg(framedMsg, unframedMsg, unframedMsgLen, detectType);

	// Send out all of the data
	for(x = 0; x < fullMessageLen; x++)
	{
	    do {
	    	err = ops->putc(dev, framedMsg[x]);
		} while (err == -EAGAIN);

	}
}



// Returns length of data received
int pic_recv_msg(struct udevice *dev, unsigned char *msg, msgErrorDetectionType_E detectType)
{
	struct dm_serial_ops *ops = serial_get_ops(dev);
	// Do to some requests to the PIC taking upto 75 ms
	// the max timeout has been set to 225 ms to be safe.
#define PIC_COMM_TIMEOUT 22500
    unsigned char  *msgBuf  = msg;
    char receivedByte		= 0;
    int time 				= PIC_COMM_TIMEOUT;
    int timeout 			= 0;
    int escaped 			= 0;
    int receiving 			= 0;
    unsigned char cksum 	= 0;
    uint16_t calculatedCrc 	= 0;
    uint16_t storedCrc	    = 0;
    int receivedFullMessage = 0;
    int numReceivedBytes	= 0;

    while(1) {
        if (--time == 0) {
            timeout = 1;
            break;
        }

        if(!ops->pending(dev, true)) {
        	udelay(10); /* 10us sleep to keep up with 1Mb baud rate */
        	continue;
        }
        receivedByte = ops->getc(dev);

        time = PIC_COMM_TIMEOUT;

        if (!receiving) {
            if (receivedByte == STX) {
                receiving = 1;
            }
        }
        else {
            *msgBuf++ = receivedByte;
            numReceivedBytes++;
            if (!escaped) {
                if (receivedByte == ETX) {
                	msgBuf--; // Don't want ETX in buffer
                	numReceivedBytes--;
                	receivedFullMessage = 1;
                    break;
                }
                else if (receivedByte == DLE) {
                	msgBuf--; // backup! we don't want this char
                	numReceivedBytes--;
                    escaped = 1;
                    continue;
                }
            }
            else {
                escaped = 0;
            }
            cksum += receivedByte;
        }
    }

    // If a message was received validate the message
    if(receivedFullMessage == 1)
    {
        switch(detectType) {
        case msgErrorDetectionType_Checksum:
            // Calculate the checksum
            if (cksum != 0)	{
                printf("Checksum is %02X instead of zero -- packet dropped\n", cksum);
                return -1;
            }
            break;
        case msgErrorDetectionType_Crc:
            // Calculate CRC
            calculatedCrc = CalculateCrc16_CCITT(msg, numReceivedBytes - 2);

            // Store CRC
            storedCrc  = msg[numReceivedBytes - 2];
            storedCrc |= (msg[numReceivedBytes - 1] << 8);
            if(storedCrc != calculatedCrc) {
                printf("Expected CRC is 0x%X, received CRC is 0x%X  -- packet dropped\n", calculatedCrc, storedCrc);
                return -1;
            }
            break;
        }
    }

	if (timeout)
	{
	    printf("pic_recv_message timeout with %d chars received\n", numReceivedBytes);
	    return -1;
	}

	return numReceivedBytes;
}
