//*****************************************************************************
// Disclosure:
// Copyright(C) 2010 Systems and Software Enterprises, Inc. (dba IMS)
// ALL RIGHTS RESERVED.
// The contents of this medium may not be reproduced in whole or part without
// the written consent of IMS, except as authorized by section 117 of the U.S.
// Copyright law.
//*****************************************************************************

#ifndef ZII_PIC_COMM_BASE_H
#define ZII_PIC_COMM_BASE_H

typedef enum {
	msgErrorDetectionType_Checksum = 0,
	msgErrorDetectionType_Crc
} msgErrorDetectionType_E;

void pic_send_msg(struct udevice *dev, uint8_t *msg, int len, msgErrorDetectionType_E detectType);
int  pic_recv_msg(struct udevice *dev, unsigned char *msg, msgErrorDetectionType_E detectType);
void pic_uart_init(struct udevice *dev, int baudRate);

#endif // ZII_PIC_COMM_BASE_H
