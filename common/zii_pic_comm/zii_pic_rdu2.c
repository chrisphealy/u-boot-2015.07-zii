//*****************************************************************************
// Disclosure:
// Copyright(C) 2010 Systems and Software Enterprises, Inc. (dba IMS)
// ALL RIGHTS RESERVED.
// The contents of this medium may not be reproduced in whole or part without
// the written consent of IMS, except as authorized by section 117 of the U.S.
// Copyright law.
//*****************************************************************************

#include <common.h>
#include <dm.h>
#include <command.h>
#include <stdio_dev.h>
#include "zii_pic_rdu2.h"
#include "zii_pic_comm_base.h"
#include "zii_pic_rdu2_eeprom_map.h"
#include "../zii_platforms/zii_rdu2_lcd_type.h"

extern uint16_t    CalculateCrc16_CCITT(const uint8_t * buf, uint32_t len);

#ifndef CONFIG_ZII_RDU2_PIC_SERIAL_INDEX
#error "define CONFIG_ZII_RDU2_PIC_SERIAL_INDEX to use the RDU2 PIC interface"
#endif

#define RDU2_PIC_BAUD_RATE 1000000

static struct udevice *theDevice;
static int picProbed = 0;

int rdu2_pic_get_fw_version (char* fwVersion) {
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Rsp_Generic_S.command = CMD_GET_FW_VERSION;

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 2, msgErrorDetectionType_Crc);
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);

    if (len > 0) {
    	sprintf(fwVersion, "IMS-RAV-RD2X-%02d%02d%02d.%c%c", bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_hw,
    			bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_major_ver,
				bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_minor_ver,
				bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_letter_1,
				bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_letter_2);
    	return 0;
    }

    return -1;
}

int cmd_pic_get_fw_version (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
	char fwVersion[256] = {0};
	int ret =rdu2_pic_get_fw_version(fwVersion);
	if(ret != 0) {
		return ret;
	}

	printf("%s\n", fwVersion);

	return 0;
}

U_BOOT_CMD(
    pic_get_fw_version ,    3,    0,     cmd_pic_get_fw_version,
    "Get FW Version of Supervisory Processor",
    ""
);

int rdu2_pic_get_bl_version (char* blVersion) {
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
    	rdu2_pic_probe();
    }

    bufToPic.Cmd_Rsp_Generic_S.command = CMD_GET_BL_VERSION;

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 2, msgErrorDetectionType_Crc);
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);

    if (len > 0) {
    	sprintf(blVersion,  "IMS-RAV-RD2X-%02d%02d%02d.%c%c", bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_hw,
    			bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_major_ver,
				bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_minor_ver,
				bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_letter_1,
				bufFromPic.Rsp_Get_Fw_Bl_Version_S.part_num_letter_2);
    	return 0;
    }

    return -1;
}

int cmd_pic_get_bl_version (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
	char blVersion[256] = {0};
	int ret =rdu2_pic_get_bl_version(blVersion);
	if(ret != 0) {
		return ret;
	}

	printf("%s\n", blVersion);

    return 0;
}

U_BOOT_CMD(
    pic_get_bl_version ,    3,    0,     cmd_pic_get_bl_version,
    "Get BL Version of Supervisory Processor",
    ""
);


int rdu2_pic_GetRduEepromData(uint8_t eepromCmd, uint16_t pageNum, uint8_t *data)
{
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Eeprom_S.command       = eepromCmd;
    bufToPic.Cmd_Eeprom_S.w_r_flag      = 1; /* non-zero for read */
    bufToPic.Cmd_Eeprom_S.page_number   = pageNum;
    bufToPic.Cmd_Eeprom_S.ack_id        = 0;

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 5, msgErrorDetectionType_Crc);

    // Receive the Message
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);

    if(len <= 0 || bufFromPic.Rsp_Eeprom_S.success_flag == 0) {
        return 1;
    }

    //now place the data in the buffer
    memcpy(data, bufFromPic.Rsp_Eeprom_S.data, EEPROM_PAGE_LENGTH);

    return 0;
}


int rdu2_pic_SendRduEepromData(uint8_t eepromCmd, uint16_t pageNum, uint8_t *data)
{
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Eeprom_S.command       = eepromCmd;
    bufToPic.Cmd_Eeprom_S.w_r_flag      = 0; /* zero for write */
    bufToPic.Cmd_Eeprom_S.page_number   = pageNum;
    bufToPic.Cmd_Eeprom_S.ack_id        = 0;

    // Copy the data into the output buffer
    memcpy(bufToPic.Cmd_Eeprom_S.data, data, EEPROM_PAGE_LENGTH);

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 37, msgErrorDetectionType_Crc);

    // Receive the Message
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);

    if(len <= 0 || bufFromPic.Rsp_Eeprom_S.success_flag == 0) {
        return 1;
    }

    return 0;
}



void rdu2_pic_get_ip ()
{
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;
    struct in_addr ip_extracted;
    struct in_addr netmask_extracted;
    char ip_addr [22]   = {0};
    char netmask [22]   = {0};
    char env_val [100]  = {0};

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Req_Ip_Addr_S.command = CMD_REQ_IP_ADDR;
    bufToPic.Cmd_Req_Ip_Addr_S.adj_type = 0;
	bufToPic.Cmd_Req_Ip_Addr_S.lru_type = 0;


    pic_send_msg(theDevice, (uint8_t*)&(bufToPic.Contents), 4, msgErrorDetectionType_Crc);
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);

    ip_extracted.s_addr = 0;
    netmask_extracted.s_addr = 0;
    if (len > 0) {
    	ip_extracted.s_addr	=	bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_4 << 24 |
                            	bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_3 << 16 |
								bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_2 << 8  |
								bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_1;

    	netmask_extracted.s_addr = 	bufFromPic.Rsp_Req_Ip_Addr_S.mask_octet_4 << 24 |
                            		bufFromPic.Rsp_Req_Ip_Addr_S.mask_octet_3 << 16 |
									bufFromPic.Rsp_Req_Ip_Addr_S.mask_octet_2 << 8  |
									bufFromPic.Rsp_Req_Ip_Addr_S.mask_octet_1;
    }

    // If all of the octets are 0
    // then setup the board for DHCP
    if ( (bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_4 == 0) &&
         (bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_3 == 0) &&
         (bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_2 == 0) &&
         (bufFromPic.Rsp_Req_Ip_Addr_S.ip_octet_4 == 0) ){
        setenv ("ipsetup","dhcp");
    }
    else {
        ip_to_string (ip_extracted, ip_addr);
        setenv ("ipaddr",ip_addr);
        ip_to_string (netmask_extracted, netmask);
        setenv ("netmask",netmask);
        sprintf (env_val, "%s:::%s::eth1:", ip_addr, netmask);
        setenv ("ipsetup", env_val);
    }

    return;
}

// Reset Reason
int cmd_pic_get_ip_addr (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
    rdu2_pic_get_ip();

    return 0;
}


U_BOOT_CMD(
    pic_get_ip_addr ,    1,    0,     cmd_pic_get_ip_addr,
    "Get the IP address from the Microchip PIC",
    "Sets ipaddr, netmask, and ipsetup environment variables"
    ""
);



uint8_t rdu2_pic_get_reset (void) {
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Rsp_Generic_S.command = CMD_RESET_REASON;

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 2, msgErrorDetectionType_Crc);
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);

    if (len > 0) {
        return bufFromPic.Rsp_Reset_Reason_S.reset_reason;
    }

    return 0xFF;
}

// Reset Reason
int cmd_pic_get_reset (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
    uint8_t resetReason = rdu2_pic_get_reset();

    switch (resetReason) {
    case 0x0:
        setenv ("reason","Normal_Power_off");
        printf ("Reset Reason: Normal Power off\n");
        break;

    case 0x1:
        setenv ("reason","PIC_HW_Watchdog");
        printf ("Reset Reason: PIC HW Watchdog\n");
        break;

    case 0x2:
        setenv ("reason","PIC_SW_Watchdog");
        printf ("Reset Reason: PIC SW Watchdog\n");
        break;

    case 0x3:
        setenv ("reason","Input_Voltage_out_of_range");
        printf ("Reset Reason: Input Voltage out of range\n");
        break;

    case 0x4:
        setenv ("reason","Host_Requested");
        printf ("Reset Reason: Host Requested\n");
        break;

    case 0x5:
        setenv ("reason","Temperature_out_of_range");
        printf ("Reset Reason: Temperature out of range\n");
        break;

    case 0x6:
        setenv ("reason","User_Requested");
        printf ("Reset Reason: User Requested\n");
        break;

    case 0x7:
        setenv ("reason","Illegal_Configuration_Word");
        printf ("Reset Reason: Illegal Configuration Word\n");
        break;

    case 0x8:
        setenv ("reason","Illegal_Instruction");
        printf ("Reset Reason: Illegal Instruction\n");
        break;

    case 0x9:
        setenv ("reason","Illegal_Trap");
        printf ("Reset Reason: Illegal Trap\n");
        break;

    case 0xA:
        setenv ("reason","Unknown_Reset_Reason");
        printf ("Reset Reason: Unknown\n");
        break;

    default: /* Can't happen? */
        break;
    }

    return 0;
}


U_BOOT_CMD(
    pic_get_reset_reason ,    1,    0,     cmd_pic_get_reset,
    "Read the Reset Reason from the Microchip PIC",
    "Set reason environment variable"
    ""
);



int rdu2_set_boot_progress (uint16_t progress) {
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Update_Boot_Progress_Code_S.command = CMD_UPDATE_BOOT_PROGRESS_CODE;
    bufToPic.Cmd_Update_Boot_Progress_Code_S.progress_code = (uint8_t)progress;

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 3, msgErrorDetectionType_Crc);
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
    if(len > 0) {
        return 0;
    }

    return 1;
}


int rdu2_get_copper_rev (uint8_t *copper_rev) {
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Rsp_Generic_S.command = CMD_GET_BOARD_COPPER_REV;
    bufToPic.Cmd_Rsp_Generic_S.ack_id = 0;

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 2, msgErrorDetectionType_Crc);
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
    if(len > 0) {

        *copper_rev = bufFromPic.Rsp_Get_Copper_Rev_S.raw_copper_value;

        return 0;
    }

    return 1;
}


int rdu2_pic_incrementNumFailedBoots(void) {

    uint8_t data[EEPROM_PAGE_LENGTH] 	= {0};
    uint8_t numBoots        			= 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    // Read out Page 5 of the RDU EEPROM
    // It contains the UINT16 for number of boot failures
    if(rdu2_pic_GetRduEepromData(CMD_RDU_EEPROM, rmbEeprom_CurFailedBootAttempts.page, data) != 0) {
        // Error, was unable to read data from the RDU EEPROM
        return 1;
    }

    //UINT8 from the EEPROM
    numBoots = data[rmbEeprom_CurFailedBootAttempts.offset];

    // Check to see if the board has been initalized
    // If  not set the failed count to 0;
    if(numBoots == 0xFF) {
        numBoots = 0;
    }

    // Increment the number of failed boots
    numBoots++;

    // If the number of failed boots is about to wrap
    // around don't save the new value as
    // we want to see that a insane amount of
    // bad boots has already happened
    if(numBoots >= 0xFE) {
        // Just return success
        return 0;
    }

    // Split the UINT16 back into bytes to be written to the EEPROM
    data[rmbEeprom_CurFailedBootAttempts.offset] = numBoots;

    return rdu2_pic_SendRduEepromData(CMD_RDU_EEPROM, rmbEeprom_CurFailedBootAttempts.page, data);
}



int rdu2_pic_getNumFailedBoots(uint8_t *boots) {
	uint8_t data[EEPROM_PAGE_LENGTH]  = {0};

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	// Read out Page 4 of the RDU EEPROM
	// It contains the UINT16 for number of boot failures
	if(rdu2_pic_GetRduEepromData(CMD_RDU_EEPROM, rmbEeprom_CurFailedBootAttempts.page, data) != 0) {
		// Error, was unable to read data from the RDU EEPROM
		printf("[ERROR] Unable to read from the EEPROM\n");
		return 1;
	}

	//UINT8 from the EEPROM
	if(data[rmbEeprom_CurFailedBootAttempts.offset] ==  0xFF) {
		*boots = 0;
	}
	else {
		*boots = data[rmbEeprom_CurFailedBootAttempts.offset];
	}

	return 0;
}


int rdu2_pic_set_wdt (int enable, int timeout) {
    Rdu2PicCommPacket_S bufToPic;
    Rdu2PicCommPacket_S bufFromPic;
    int len = 0;

    if(picProbed == 0) {
		rdu2_pic_probe();
	}

    bufToPic.Cmd_Sw_Wdt_S.command = CMD_SW_WDT ;
    bufToPic.Cmd_Sw_Wdt_S.ack_id = 0;

    bufToPic.Cmd_Sw_Wdt_S.wdt_en = enable;
    bufToPic.Cmd_Sw_Wdt_S.wdt_timeout = (uint8_t)timeout;

    pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 4, msgErrorDetectionType_Crc);
    len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);

    return 0;
}

int cmd_pic_set_wdt (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
    int enable  = 0;
    int timeout = 0;

    // Check the number on command line arguments
    if (argc != 3) {
        cmd_usage(cmdtp);
        return 1;
    }

    enable  = simple_strtol(argv[1], NULL, 10);
    timeout = simple_strtol(argv[2], NULL, 10);

    rdu2_pic_set_wdt(enable, timeout);

    return 0;
}

U_BOOT_CMD(
    pic_set_wdt ,    3,    0,     cmd_pic_set_wdt,
    "Set the Watchdog Timer (WDT) on the Microchip PIC",
    "1 12 (Enable Watchdog with Timeout value of 12 seconds)\n"
    "1 300 (Enable Watchdog with Timeout value of 300 seconds)\n"
    "0 60 (Disable Watchdog and ignore Timeout parameter)\n"
    "Parameter 1: 0 = disable, 1 = enable, 2 = enable in debug mode\n"
    "Parameter 2: Timeout Value in second, range 12-300\n"
    "Timeout value Min and Max depends on board type\n"
    ""
);


int rdu2_pic_get_boot_device() {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Boot_Source_S.command = CMD_BOOT_SOURCE;
	bufToPic.Cmd_Boot_Source_S.ack_id = 0;

	bufToPic.Cmd_Boot_Source_S.set_get = 0;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 3, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		return -1;
	}

	return bufFromPic.Rsp_Boot_Source_S.boot_source;
}

int cmd_pic_get_boot_device (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int boot_source;

	if (argc != 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	boot_source = rdu2_pic_get_boot_device ();
	if(boot_source == -1) {
		printf("Error getting boot source\n");
		return 1;
	}

	printf("Current Boot Device: ");
	switch(boot_source) {
	case 0:
		printf("SD\n");
		break;
	case 1:
		printf("eMMC\n");
		break;
	case 2:
		printf("SPI NOR\n");
		break;
	default:
		printf("Unknown\n");
	}

	return 0;
}

U_BOOT_CMD(
	pic_get_boot_device ,    1,    0,     cmd_pic_get_boot_device,
	"Gets the current boot source from the PIC",
	""
);



int rdu2_pic_set_boot_device(uint8_t boot_device) {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Boot_Source_S.command = CMD_BOOT_SOURCE;
	bufToPic.Cmd_Boot_Source_S.ack_id = 0;
	bufToPic.Cmd_Boot_Source_S.set_get = 1;
	bufToPic.Cmd_Boot_Source_S.boot_source = boot_device;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 4, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		printf("Error setting boot source\n");
	}

	return 0;
}

int cmd_pic_set_boot_device (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
	int theDevice = 0;

	if (argc != 2) {
		cmd_usage(cmdtp);
		return 1;
	}

	theDevice = simple_strtol(argv[1], NULL, 10);

	rdu2_pic_set_boot_device (theDevice);

	return 0;
}

U_BOOT_CMD(
	pic_set_boot_device ,    2,    0,     cmd_pic_set_boot_device,
	"Sets the current boot source via the PIC",
	"0 (Set Boot device to 0 (SD) )\n"
	"1 (Set Boot device to 1 (eMMC) )\n"
	"2 (Set Boot device to 2 (NOR) )\n"
	""
);

MacErrorCodes_E rdu2_pic_getMacAddress(NicIds_E nic_id, uint8_t mac_addr[6]) {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int numBytesReceived = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Mac_Address_S.command = CMD_MAC_ADDRESS;
	bufToPic.Cmd_Mac_Address_S.ack_id  = 0x00;
	bufToPic.Cmd_Mac_Address_S.nic_id  = nic_id;
	bufToPic.Cmd_Mac_Address_S.set_get = 0;

	bufToPic.Cmd_Mac_Address_S.mac_addr_0 = 0;
	bufToPic.Cmd_Mac_Address_S.mac_addr_1 = 0;
	bufToPic.Cmd_Mac_Address_S.mac_addr_2 = 0;
	bufToPic.Cmd_Mac_Address_S.mac_addr_3 = 0;
	bufToPic.Cmd_Mac_Address_S.mac_addr_4 = 0;
	bufToPic.Cmd_Mac_Address_S.mac_addr_5 = 0;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 10, msgErrorDetectionType_Crc);
	numBytesReceived = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(numBytesReceived <= 0 || bufFromPic.Rsp_Mac_Address_S.command != RSP_MAC_ADDRESS) {
		printf("Error getting MAC address\n");
	}

	mac_addr[0] = bufFromPic.Rsp_Mac_Address_S.mac_addr_5;
	mac_addr[1] = bufFromPic.Rsp_Mac_Address_S.mac_addr_4;
	mac_addr[2] = bufFromPic.Rsp_Mac_Address_S.mac_addr_3;
	mac_addr[3] = bufFromPic.Rsp_Mac_Address_S.mac_addr_2;
	mac_addr[4] = bufFromPic.Rsp_Mac_Address_S.mac_addr_1;
	mac_addr[5] = bufFromPic.Rsp_Mac_Address_S.mac_addr_0;

	return bufFromPic.Rsp_Mac_Address_S.status;
}

int cmd_pic_get_mac_address (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
	int nic_id = 0;
	uint8_t mac_addr[6] = {0};

	if (argc != 2) {
		cmd_usage(cmdtp);
		return 1;
	}

	nic_id = simple_strtol(argv[1], NULL, 10);

	rdu2_pic_getMacAddress((NicIds_E)nic_id, mac_addr);

	printf("%02X:%02X:%02X:%02X:%02X:%02X\n",   mac_addr[0],
												mac_addr[1],
												mac_addr[2],
												mac_addr[3],
												mac_addr[4],
												mac_addr[5]);

	return 0;
}

U_BOOT_CMD(
	pic_get_mac_addr , 2, 0, cmd_pic_get_mac_address,
	"Retreives the MAC address from the PIC",
	"0: 100 Mbit i.MX6 FEC\n"
	"1: 1 Gbit i210 NIC\n"
	""
);

int rdu2_pic_reboot() {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Pic_Reset_S.command = CMD_PIC_RESET ;
	bufToPic.Cmd_Pic_Reset_S.ack_id = 0;
	bufToPic.Cmd_Pic_Reset_S.reset_byte = 1;
	bufToPic.Cmd_Pic_Reset_S.requested_reason = 0;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 4, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		printf("Error setting boot source\n");
	}

	return 0;
}

int cmd_pic_reset (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {

	rdu2_pic_reboot();

	return 0;
}

U_BOOT_CMD(
	pic_reset ,    2,    0,     cmd_pic_reset,
	"Reboots the unit",
	""
);

int rdu2_pic_get_display_type() {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;
	char str[3] = {0};
	const char *panel = getenv("panel");
	const char *display = getenv("display");

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Lcd_Type_S.command = CMD_LCD_TYPE;
	bufToPic.Cmd_Lcd_Type_S.ack_id = 0;
	bufToPic.Cmd_Lcd_Type_S.set_get = 0;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 3, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		return -1;
	}

	sprintf(str, "%d", bufFromPic.Rsp_Lcd_Type_S.lcd_type);
	if(bufFromPic.Rsp_Lcd_Type_S.lcd_type < DISPLAY_TYPE_COUNT) {

		setenv ("display", str);
		setenv ("panel", displayData[bufFromPic.Rsp_Lcd_Type_S.lcd_type].display_name);
	}
	else {
		/* Default to Innolux 10.1" */
		sprintf(str, "%d", DISPLAY_TYPE_AUO_10_1);
		setenv ("display", str);
		setenv ("panel", displayData[DISPLAY_TYPE_AUO_10_1].display_name);

	}

	if(strcmp(panel, displayData[DISPLAY_TYPE_AUO_10_1].display_name) != 0 ||
		strcmp(display, str) != 0) {
		saveenv();
	}

	return bufFromPic.Rsp_Lcd_Type_S.lcd_type;
}

int cmd_pic_get_display_type (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int display_type;

	if (argc != 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	display_type = rdu2_pic_get_display_type ();
	if(display_type == -1) {
		printf("Error getting display type\n");
		return 1;
	}

	return 0;
}

U_BOOT_CMD(
	pic_get_display_type ,    1,    0,     cmd_pic_get_display_type,
	"Gets the display type from the PIC",
	""
);


int rdu2_pic_send_display_data_stable() {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Rsp_Generic_S.command = CMD_LVDS_DATA_STABLE;
	bufToPic.Cmd_Rsp_Generic_S.ack_id = 0;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 2, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		return -1;
	}

	return 0;
}

int cmd_pic_send_display_data_stable (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret;

	if (argc != 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	ret = rdu2_pic_send_display_data_stable ();
	if(ret == -1) {
		printf("Error sending display data stable message\n");
		return 1;
	}

	return 0;
}

U_BOOT_CMD(
	pic_send_display_stable ,    1,    0,     cmd_pic_send_display_data_stable,
	"Tells the PIC that display data output is stable",
	""
);


int rdu2_pic_get_voltage(RmbDebVolt_E id, uint32_t *voltage) {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Get_VoltCurReading.command = CMD_GET_VOLTAGE;
	bufToPic.Cmd_Get_VoltCurReading.ack_id = 0;
	bufToPic.Cmd_Get_VoltCurReading.volt_cur_id = (uint8_t)id;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 3, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		return -1;
	}

	*voltage = bufFromPic.Rsp_Get_VoltCurReading.reading;

	return 0;
}

int cmd_pic_get_all_voltage (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret;
	uint32_t voltage;
	uint8_t id;

	if (argc != 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	for(id = 0; id < VOLT_NUM_RAILS; id++) {
			ret = rdu2_pic_get_voltage(id, &voltage);
			if(ret != 0) {
				return -1;
			};

	        switch(id) {
	        case VOLT_RMB_3V3_PMIC:
	            printf("RMB 3V3_PMIC: ");
	            break;
	        case VOLTRMB_3V3_MCU:
	            printf("RMB 3V3_MCU: ");
	            break;
	        case VOLT_RMB_5V_MAIN:
	            printf("RMB 5V_MAIN: ");
	            break;
	        case VOLT_RMB_12V_MAIN:
	            printf("RMB 12V_MAIN: ");
	            break;
	        case VOLT_RMB_28V_FIL:
	            printf("RMB 28V_FIL: ");
	            break;
	        case VOLT_RMB_28V_HOTSWAP:
	            printf("RMB 28V_HOTSWAP: ");
	            break;
	        case VOLT_DEB_1V8:
	            printf("DEV 1V8: ");
	            break;
	        case VOLT_DEB_3V3:
	            printf("DEB 3V3: ");
	            break;
	        case VOLT_DEB_28V_DEB:
	            printf("DEB 28V_DEB: ");
	            break;
	        case VOLT_DEB_28V_RDU:
	            printf("DEB 28V_RDU: ");
	            break;
	        default:
	            printf("UNKNOWN: ");
	            break;
	        }

	        // print 32.8 fixed point number
            printf("%d.%02d\n", voltage / 256, voltage % 256 * 100 / 256);
	    }

	return 0;
}

U_BOOT_CMD(
	pic_get_all_voltage ,    1,    0,     cmd_pic_get_all_voltage,
	"Retrieves all of the voltage readings from the PIC",
	""
);


int rdu2_pic_get_temperature(RmbDebTemp_E id, int32_t *temperature) {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Get_VoltCurReading.command = CMD_GET_TEMPERATURE;
	bufToPic.Cmd_Get_VoltCurReading.ack_id = 0;
	bufToPic.Cmd_Get_VoltCurReading.volt_cur_id = (uint8_t)id;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 3, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		return -1;
	}

	// Change 32.1 fixed point to 32.8
	*temperature = bufFromPic.Rsp_Get_VoltCurReading.reading * 128;

	return 0;
}

int cmd_pic_get_all_temperature (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret;
	int32_t temperature;
	uint8_t id;

	if (argc != 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	for(id = 0; id < TEMPERATURE_NUM_SENSORS; id++) {
			ret = rdu2_pic_get_temperature(id, &temperature);
			if(ret != 0) {
				return -1;
			};

			switch(id) {
			case TEMPERATURE_RMB:
				printf("RMB: ");
				break;
			case TEMPERATURE_DEB:
				printf("DEB: ");
				break;
			default:
				printf("UNKNOWN: ");
				break;
			}

	        printf("%d.%02d\n", temperature / 256, temperature % 256 * 100 / 256);
	    }

	return 0;
}

U_BOOT_CMD(
	pic_get_all_temperature ,    1,    0,     cmd_pic_get_all_temperature,
	"Retrieves all of the temperature readings from the PIC",
	""
);

int rdu2_pic_get_current(RmbDebCur_E id, uint32_t *current) {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Get_VoltCurReading.command = CMD_GET_CURRENT;
	bufToPic.Cmd_Get_VoltCurReading.ack_id = 0;
	bufToPic.Cmd_Get_VoltCurReading.volt_cur_id = (uint8_t)id;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 3, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		return -1;
	}

	*current = bufFromPic.Rsp_Get_VoltCurReading.reading;

	return 0;
}

int cmd_pic_get_all_current (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret;
	uint32_t current;
	uint8_t id;

	if (argc != 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	for(id = 0; id < CUR_NUM_RAILS; id++) {
			ret = rdu2_pic_get_current(id, &current);
			if(ret != 0) {
				return -1;
			};

			switch(id) {
			case CUR_RMB_28V_HOTSWAP:
				printf("RMB 28V_HOTSWAP: ");
				break;
			default:
				printf("UNKNOWN: ");
				break;
			}

	        printf("%d\n", current);
	    }

	return 0;
}

U_BOOT_CMD(
	pic_get_all_current ,    1,    0,     cmd_pic_get_all_current,
	"Retrieves all of the current readings from the PIC",
	""
);



const char * const rdu2PinNames[] = {
    "STOWAGE_DETECT",
    "RMB_LED_RED",
    "RMB_LED_GREEN",
    "LCD_BKLT_EN",
    "3V3_DISPLAY_EN",
    "28V_ALERT_N",
    "RMB_LED_BLUE",
    "3V3_DISPLAY_PGOOD",
    "POR_B",
    "SW_TEST1",
    "SW_TEST2",
    "DEBUG_232_EN",
    "JTAG_OE1_N",
    "12V_MAIN_PGOOD",
    "RJU_12V_OC_N",
    "3V3_SSD_PGOOD",
    "RJU_12V_PGOOD",
    "GEN_3V3_PGOOD",
    "TMP_SNS_2_FLT_N",
    "EXT_RESET_IN_N",
    "RJU_12V_EN",
    "TP83",
    "TP63",
    "TP62",
    "HW_RESET_SWITCH_N",
    "CONNECTOR_LED_N",
    "PWR_LED1_N",
    "PWR_LED2_N",
    "28V_SHUTDOWN_N",
    "DC_EN_PMIC_3V3",
    "DC_EN_3V3_SSD",
    "12V_BCKLIGHT_EN",
    "DC_EN_5V",
    "PMIC_PWRON",
    "PIC_RST_EDP_N",
    "MX6_TO_PIC_INT_N",
    "TEST_MODE_PIC_N",
    "READING_LIGHT_DIM",
    "PIC_SSD_DEV_SLP",
    "PIC_POWER_LOSS_INT_N",
    "EDP_1V2_EN",
    "12V_BACKLIGHT_PGOOD",
    "12V_BACKLIGHT_OC_N",
    "READING_LIGHT_EN",
    "MOOD_LED_GREEN",
    "MOOD_LED_RED",
    "MOOD_LED_BLUE"
};

int rdu2_pic_get_gpio_states(uint16_t gpio_states[4]) {
	Rdu2PicCommPacket_S bufToPic;
	Rdu2PicCommPacket_S bufFromPic;
	int len = 0;
	int x = 0;

	if(picProbed == 0) {
		rdu2_pic_probe();
	}

	bufToPic.Cmd_Rsp_Generic_S.command = CMD_GPIO_STATE;
	bufToPic.Cmd_Rsp_Generic_S.ack_id = 0;

	pic_send_msg(theDevice, (uint8_t *)&(bufToPic.Contents), 2, msgErrorDetectionType_Crc);
	len = pic_recv_msg(theDevice, (uint8_t *)&(bufFromPic.Contents), msgErrorDetectionType_Crc);
	if(len <= 0) {
		return -1;
	}

	for(x = 0; x < 4; x++) {
		gpio_states[x] = bufFromPic.Rsp_Gpio_State_S.gpio_state[x];
	}

	return 0;
}

int cmd_pic_get_gpio_states (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret;
	uint16_t gpio_states[4];
	uint16_t shift = 0;
	uint16_t index = 0;
	int x = 0;

	if (argc != 1) {
		cmd_usage(cmdtp);
		return 1;
	}

	ret = rdu2_pic_get_gpio_states(gpio_states);
	if(ret != 0) {
		return -1;
	};

	printf("PIC GPIO Values:\n");
	for(x = 0; x < 47; x++) {
		printf("%d : %s\n", (uint8_t)((gpio_states[index] >> shift++) & 0x1), rdu2PinNames[x]);
		if(shift == 16) {
			shift = 0;
			index++;
		}
	}

	return 0;
}

U_BOOT_CMD(
	pic_get_gpio_states ,    1,    0,     cmd_pic_get_gpio_states,
	"Retrieves the states of all of the GPIOs on the PIC",
	""
);




int rdu2_pic_probe() {
	int ret = uclass_get_device(UCLASS_SERIAL, CONFIG_ZII_RDU2_PIC_SERIAL_INDEX, &theDevice);
	if( ret != 0) {
		printf("No Device By that name\n");
		return ret;
	}

	if(picProbed == 1) {
		return 0;
	}

	// Set up the serial port
	pic_uart_init(theDevice, RDU2_PIC_BAUD_RATE);
	picProbed = 1;

	return 0;
}



int rdu2_pic_GetRduEepromStringDataWithCrcCheck(uint16_t pageNum, char *char_data)
{
    uint8_t *data = (uint8_t *)char_data;

    uint16_t i;
    int status = rdu2_pic_GetRduEepromData(CMD_RDU_EEPROM, pageNum, data );

    if (0 != status) {
        memset(data, 0, EEPROM_PAGE_LENGTH);
        strcpy( (char *)data, "Read Failed");
        return 1;
    }

    uint16_t calcCrc = CalculateCrc16_CCITT( data, EEPROM_PAGE_LENGTH - 2);
    uint16_t readCrc = (data[EEPROM_PAGE_LENGTH - 2] << 0)
                     + (data[EEPROM_PAGE_LENGTH - 1] << 8);
    if (readCrc != calcCrc) {
        memset(data, 0, EEPROM_PAGE_LENGTH);
        sprintf( (char *)data, "Invalid");
        return 1;
    }

    uint16_t length = data[0];
    if (length > EEPROM_PAGE_LENGTH - 3) {
        memset(data, 0, EEPROM_PAGE_LENGTH);
        sprintf( (char *)data, "Invalid.");
        return 1;
    }

    for (i=0; i<length; ++i) {
        char theChar = data[i+1];
        if (0 == theChar) {
            data[i] = theChar;
        }
        if (0x20 <= theChar && theChar <= 0x7E) {
            data[i] = theChar;
        }
        else {
            data[i] = '?';
        }
    }
    // Add zero term
    data[i++] = 0;
    return 0;
}



int rdu2_pic_get_partnumber(char *part_number)
{
    return rdu2_pic_GetRduEepromStringDataWithCrcCheck( rmbEeprom_LruPartNumber.page, part_number);
}


int cmd_pic_get_partnumber (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret;
    char part_number[EEPROM_PAGE_LENGTH];

    if (argc != 1) {
        cmd_usage(cmdtp);
        return 1;
    }

    ret = rdu2_pic_get_partnumber( part_number);
    printf("%s\n", part_number);

    return 0;
}

U_BOOT_CMD(
    pic_get_partnumber ,    1,    0,     cmd_pic_get_partnumber,
    "Retrieves the partnumber from the PIC",
    ""
);


int rdu2_pic_get_serial_number(char *serial_number)
{
    return rdu2_pic_GetRduEepromStringDataWithCrcCheck( rmbEeprom_LruSerialNumber.page, serial_number);
}


int cmd_pic_get_serialnumber (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret;
    char serial_number[EEPROM_PAGE_LENGTH];

    if (argc != 1) {
        cmd_usage(cmdtp);
        return 1;
    }

    ret = rdu2_pic_get_serial_number( serial_number);
    printf("%s\n", serial_number);

    return 0;
}

U_BOOT_CMD(
    pic_get_serialnumber ,    1,    0,     cmd_pic_get_serialnumber,
    "Retrieves the serialnumber from the PIC",
    ""
);



int rdu2_pic_get_hardware_version(char *hardware_version)
{
    return rdu2_pic_GetRduEepromStringDataWithCrcCheck( rmbEeprom_BoardCurrentRev.page, hardware_version);
}


int cmd_pic_get_hardwareVersion (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
    int ret;
    char hardware_version[EEPROM_PAGE_LENGTH];

    if (argc != 1) {
        cmd_usage(cmdtp);
        return 1;
    }

    ret = rdu2_pic_get_hardware_version( hardware_version);
    printf("%s\n", hardware_version);

    return 0;
}

U_BOOT_CMD(
    pic_get_hardwareVersion ,    1,    0,     cmd_pic_get_hardwareVersion,
    "Retrieves the hardwareVersion from the PIC",
    ""
);








