//******************************************************************************
// Copyright ï¿½ 2015 Zodiac Inflight Innovations, a subsidiary of Zodiac
// Aerospace.  All rights reserved.  The contents of this medium are
// confidential and proprietary to Zodiac Aerospace, and shall not be
// disclosed, disseminated, copied or used except as expressly authorized in
// writing by Zodiac Inflight Innovations.
//******************************************************************************


#ifndef ZII_PIC_RDU2_H
#define ZII_PIC_RDU2_H

// The following define is to be updated each time the
// communication structure or ICD is changed
#define NIUPIC_HOST_VERSION         0x01

typedef enum {
    CMD_SW_WDT                    = 0xA1,
    CMD_PET_WDT                   = 0xA2,
    CMD_DDS_EEPROM                = 0xA3,
    CMD_RDU_EEPROM                = 0xA4,
    CMD_LCD_BACKLIGHT             = 0xA6,
    CMD_PIC_RESET                 = 0xA7,
    CMD_RESET_REASON              = 0xA8,
    CMD_PWR_LED                   = 0xA9,
    CMD_SET_APP_BUTTON            = 0xAA,
    CMD_SEND_STATUS               = 0xAC,
    CMD_SET_PIC_CONSOLE           = 0xAD,
    CMD_RESET_ETH_SWITCH          = 0xAF,
    CMD_JUMP_TO_BTLDR             = 0xB0,
    CMD_BOOTLOADER                = 0xB1,
    CMD_REQ_IP_ADDR               = 0xB2,
    CMD_SUCCESSFUL_BOOT           = 0xB3,
    CMD_TIMING_TEST               = 0xB4,
    CMD_UPDATE_BOOT_PROGRESS_CODE = 0xB5,
    CMD_REQ_COPPER_REV            = 0xB6,
    CMD_CHANGE_PERIPHERAL_POWER   = 0xB7,
    CMD_UPDATE_LED_BRIGHTNESS     = 0xB8,
    CMD_REQ_SILICON_REV           = 0xB9,
    CMD_GET_I2C_DEVICE_STATUS     = 0xBA,
    CMD_CONTROL_EVENTS            = 0xBB,
    CMD_RJU_POWER                 = 0xBC,
    CMD_DELAYED_RESET             = 0xBD,
    CMD_LVDS_DATA_STABLE          = 0xBE,
    CMD_GET_FW_VERSION            = 0x20,
    CMD_GET_BL_VERSION            = 0x21,
    CMD_GET_VOLTAGE               = 0x22,
    CMD_GET_CURRENT               = 0x23,
    CMD_GET_TEMPERATURE           = 0x24,
    CMD_GET_OPERATIONAL_MODE      = 0x25,
    CMD_BOOT_SOURCE               = 0x26,
    CMD_GET_DISCRETE_STATE        = 0x27,
    CMD_LED_CTRL                  = 0x28,
    CMD_GET_INTERRUPT_REASON      = 0x29,
    CMD_GET_ETC                   = 0x2A,
    CMD_GET_BOARD_COPPER_REV      = 0x2B,
    CMD_TEST_COMPONENT            = 0x2C,
    CMD_MAC_ADDRESS               = 0x2D,
    CMD_LCD_TYPE                  = 0x2E,
    CMD_GPIO_STATE                = 0x2F,


    RSP_STATUS                    = 0xC0,
    RSP_SW_WDT                    = 0xC1,
    RSP_PET_WDT                   = 0xC2,
    RSP_DDS_EEPROM                = 0xC3,
    RSP_RDU_EEPROM                = 0xC4,
    RSP_RS_485                    = 0xC5,
    RSP_LCD_BACKLIGHT             = 0xC6,
    RSP_PIC_RESET                 = 0xC7,
    RSP_RESET_REASON              = 0xC8,
    RSP_PIC_PWR_LED               = 0xC9,
    RSP_SET_APP_BUTTON            = 0xCA,
    RSP_WIFI                      = 0xCB,
    RSP_SEND_STATUS               = 0xCC,
    RSP_SET_PIC_CONSOLE           = 0xCD,
    RSP_SWITCH_EEPROM             = 0xCE,
    RSP_RESET_ETH_SWITCH          = 0xCF,
    RSP_JUMP_TO_BTLDR             = 0xD0,
    RSP_BOOTLOADER                = 0xD1,
    RSP_REQ_IP_ADDR               = 0xD2,
    RSP_SUCCESSFUL_BOOT           = 0xD3,
    RSP_TIMING_TEST               = 0xD4,
    RSP_UPDATE_BOOT_PROGRESS_CODE = 0xD5,
    RSP_REQ_COPPER_REV            = 0xD6,
    RSP_CHANGE_PERIPHERAL_POWER   = 0xD7,
    RSP_UPDATE_LED_BRIGHTNESS     = 0xD8,
    RSP_REQ_SILICON_REV           = 0xD9,
    RSP_GET_I2C_DEVICE_STATUS     = 0xDA,
    RSP_CONTROL_EVENTS            = 0xDB,
    RSP_RJU_POWER                 = 0xDC,
    RSP_DELAYED_RESET             = 0xDD,
    RSP_LVDS_DATA_STABLE          = 0xDE,
    RSP_GET_FW_VERSION            = 0x60,
    RSP_GET_BL_VERSION            = 0x61,
    RSP_GET_VOLTAGE               = 0x62,
    RSP_GET_CURRENT               = 0x63,
    RSP_GET_TEMPERATURE           = 0x64,
    RSP_GET_OPERATIONAL_MODE      = 0x65,
    RSP_BOOT_SOURCE               = 0x66,
    RSP_GET_DISCRETE_STATE        = 0x67,
    RSP_LED_CTRL                  = 0x68,
    RSP_GET_INTERRUPT_REASON      = 0x69,
    RSP_GET_ETC                   = 0x6A,
    RSP_GET_BOARD_COPPER_REV      = 0x6B,
    RSP_TEST_COMPONENT            = 0x6C,
    RSP_MAC_ADDRESS               = 0x6D,
    RSP_LCD_TYPE                  = 0x6E,
    RSP_GPIO_STATE                = 0x6F,

} Rdu2PicComm_E;


typedef enum {
    DEB_NET_PORT_LEFT = 0,
    DEB_NET_PORT_AUX,
    DEB_NET_PORT_RIGHT,
    DEB_NET_PORT_CPU
} DebNetPorts_E;

typedef enum {
    VOLT_RMB_3V3_PMIC = 0,
    VOLTRMB_3V3_MCU,
    VOLT_RMB_5V_MAIN,
    VOLT_RMB_12V_MAIN,
    VOLT_RMB_28V_FIL,
    VOLT_RMB_28V_HOTSWAP,
    // All DEB voltages are via an ADT7411
    VOLT_DEB_1V8,
    VOLT_DEB_3V3,
    VOLT_DEB_28V_DEB,
    VOLT_DEB_28V_RDU,

    VOLT_NUM_RAILS
} RmbDebVolt_E;

typedef enum {
    CUR_RMB_28V_HOTSWAP = 0,

    CUR_NUM_RAILS
} RmbDebCur_E;

typedef enum {
    TEMPERATURE_RMB = 0,
    TEMPERATURE_DEB,

    TEMPERATURE_NUM_SENSORS
} RmbDebTemp_E;

typedef enum {
    NIC_ID_100_MX6 = 0,
    NIC_ID_1000_I210
} NicIds_E;

typedef enum {
    MAC_ERROR_OK = 0,
    MAC_ERROR_PRI_BAK_UNINIT,
    MAC_ERROR_PRI_UNINIT_BAK_OK,
    MAC_ERROR_PRI_BAK_CRC_BAD,
    MAC_ERROR_PRI_CRC_BAD_BAK_OK,
    MAC_ERROR_UNABLE_TO_WRITE,
    MAC_ERROR_UNKNOWN_NIC
} MacErrorCodes_E;




typedef union __attribute__ ((packed)) _RDU2_PIC_COMM_MESSAGE
{
        uint8_t Contents[128];

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
        } Cmd_Rsp_Generic_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   part_num_hw;
            uint16_t  part_num_major_ver;
            uint8_t   part_num_minor_ver;
            uint8_t   part_num_letter_1;
            uint8_t   part_num_letter_2;
        } Rsp_Get_Fw_Bl_Version_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   mode;
        } Rsp_Get_Operational_Mode_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   set_get;
            uint8_t   boot_source;
        } Cmd_Boot_Source_S;

         struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   boot_source;
        } Rsp_Boot_Source_S;

         struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint32_t  etc_reading;
        } Rsp_Get_Etc_Counter_S;

         struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint16_t  temp_reading;
        } Rsp_Get_Temperature_S;

         struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint16_t  voltage_reading;
        } Rsp_Get_Voltage_Reading_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint16_t  i2c_device_status;
        } Rsp_Get_I2c_Device_Status_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   wdt_en;
            uint8_t   wdt_timeout;
        } Cmd_Sw_Wdt_S;

         struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   wdt_en;
            uint8_t   wdt_timeout;
        } Rsp_Sw_Wdt_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   reset_byte;
            uint8_t   requested_reason;
        } Cmd_Pic_Reset_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   reset_reason;
            uint8_t   rcon_bits;
        } Rsp_Reset_Reason_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   w_r_flag;
            uint16_t  page_number;
            uint8_t   data[32];
        } Cmd_Eeprom_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   w_r_flag;
            uint8_t   success_flag;
            uint8_t   data[32];
        } Rsp_Eeprom_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   lru_type;
            uint8_t   adj_type;
        } Cmd_Req_Ip_Addr_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   ip_octet_1;
            uint8_t   ip_octet_2;
            uint8_t   ip_octet_3;
            uint8_t   ip_octet_4;
            uint8_t   mask_octet_1;
            uint8_t   mask_octet_2;
            uint8_t   mask_octet_3;
            uint8_t   mask_octet_4;
        } Rsp_Req_Ip_Addr_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   progress_code;
        } Cmd_Update_Boot_Progress_Code_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   enable_percentage;
            uint16_t  enable_delay;
        } Cmd_Lcd_Control_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   raw_copper_value;
        } Rsp_Get_Copper_Rev_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint32_t  device_id;
            uint32_t  silicon_rev;
        } Rsp_Get_Pic_Silicon_Rev_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   set_state;
            uint8_t   led_num;
            uint8_t   led_state;
        } Cmd_Led_Control;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   led_state;
        } Rsp_Led_Control;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   volt_cur_id;
        } Cmd_Get_VoltCurReading;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint16_t  reading;
        } Rsp_Get_VoltCurReading;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   message[62];
        } Cmd_Bootloader_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   message[62];
        } Rsp_Bootloader_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   rw_flag;
            uint8_t   port;
            uint8_t   sdu_type;
            uint8_t   location_type;
            uint8_t   numeric_value;
            uint8_t   ascii_descriptor;
            uint8_t   nw_configuration;
        } Cmd_Port_Config_Set_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   rw_flag;
            uint8_t   success_flag;
        } Rsp_Port_Config_Set_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   rw_flag;
            uint8_t   port;
        } Cmd_Port_Config_Get_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   rw_flag;
            uint8_t   success_flag;
            uint8_t   port;
            uint8_t   sdu_type;
            uint8_t   location_type;
            uint8_t   numeric_value;
            uint8_t   ascii_descriptor;
            uint8_t   nw_configuration;
        } Rsp_Port_Config_Get_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   discretes_state;
        } Rsp_Get_Discrete_State_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   set_get;
            uint8_t   led_num;
            uint8_t   led_state;
            uint8_t   brightness;
            uint8_t   red;
            uint8_t   green;
            uint8_t   blue;
        } Cmd_Led_Control_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   led_state;
            uint8_t   brightness;
            uint8_t   red;
            uint8_t   green;
            uint8_t   blue;
        } Rsp_Led_Control_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint16_t  reason;
        } Rsp_Get_Interrupt_Reason_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   nic_id;
            uint8_t   set_get;
            uint8_t   mac_addr_0;
            uint8_t   mac_addr_1;
            uint8_t   mac_addr_2;
            uint8_t   mac_addr_3;
            uint8_t   mac_addr_4;
            uint8_t   mac_addr_5;
        } Cmd_Mac_Address_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   status;
            uint8_t   mac_addr_0;
            uint8_t   mac_addr_1;
            uint8_t   mac_addr_2;
            uint8_t   mac_addr_3;
            uint8_t   mac_addr_4;
            uint8_t   mac_addr_5;
        } Rsp_Mac_Address_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint16_t  component_id;
        } Cmd_Test_Component_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   test_status;
        } Rsp_Test_Component_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   set_get;
            uint8_t   lcd_type;
        } Cmd_Lcd_Type_S;

        struct __attribute__ ((packed)) {
            uint8_t   command;
            uint8_t   ack_id;
            uint8_t   lcd_type;
        } Rsp_Lcd_Type_S;

        struct __attribute__ ((packed)) {
			uint8_t   command;
			uint8_t   ack_id;
			uint16_t  gpio_state[4];
		} Rsp_Gpio_State_S;


} Rdu2PicCommPacket_S;


int	rdu2_pic_probe(void);
void    rdu2_pic_get_ip(void);
uint8_t rdu2_pic_get_reset(void);
int     rdu2_pic_set_wdt(int enable, int timeout);
int     rdu2_pic_get_boot_device(void);
int 	rdu2_pic_set_boot_device(uint8_t boot_device);
int     rdu2_set_boot_progress(uint16_t progress);
int     rdu2_get_copper_rev(uint8_t *copper_rev);
int     rdu2_pic_incrementNumFailedBoots(void);
int     rdu2_pic_getNumFailedBoots(uint8_t *boots);
MacErrorCodes_E rdu2_pic_getMacAddress(NicIds_E nic_id, uint8_t mac_addr[6]);
int     rdu2_pic_get_display_type(void);
int     rdu2_pic_send_display_data_stable(void);
int     rdu2_pic_reboot(void);
int     rdu2_pic_get_voltage(RmbDebVolt_E id, uint32_t *voltage);
int     rdu2_pic_get_temperature(RmbDebTemp_E id, int32_t *temperature);
int     rdu2_pic_get_current(RmbDebCur_E id, uint32_t *current);
int     rdu2_pic_get_gpio_states(uint16_t gpio_states[4]);
int     rdu2_pic_get_fw_version (char* fwVersion);
int     rdu2_pic_get_bl_version (char* blVersion);
int     rdu2_pic_get_partnumber(char *part_number);
int     rdu2_pic_get_serial_number(char *serial_number);
int     rdu2_pic_get_hardware_version(char *hardware_version);


#endif // ZII_PIC_RDU2_H



