//*****************************************************************************
// Disclosure:
// Copyright(C) 2010 Systems and Software Enterprises, Inc. (dba IMS)
// ALL RIGHTS RESERVED.
// The contents of this medium may not be reproduced in whole or part without
// the written consent of IMS, except as authorized by section 117 of the U.S.
// Copyright law.
//*****************************************************************************

#ifndef ZII_PIC_RDU2_EEPROM_MAP_H
#define ZII_PIC_RDU2_EEPROM_MAP_H

#define EEPROM_PAGE_LENGTH  32

typedef struct {
    uint32_t addr;
    uint32_t length; // length in bytes, thus a uint32_t = 4
    uint16_t  page;
    uint16_t  offset;
} eepromEntry_S;

////////////////
// RMB EEPROM
////////////////                                             //  Addr     Length    Page    Offset
// PAGE 0
static const eepromEntry_S eeprom_Format                    = { 0x0000,      2,       0,     0x00 }; // F
static const eepromEntry_S rmbEeprom_LruDom                 = { 0x0010,     16,       0,     0x10 }; // D
// PAGE 1
static const eepromEntry_S rmbEeprom_LruPartNumber          = { 0x0020,     32,       1,     0x00 }; // P
// PAGE 2
static const eepromEntry_S rmbEeprom_HasTouchScreen         = { 0x0040,      1,       2,     0x00 }; // Xt
static const eepromEntry_S rmbEeprom_HasPowerLed            = { 0x0041,      1,       2,     0x01 }; // XLED
static const eepromEntry_S rmbEeprom_HasUserUsb             = { 0x0042,      1,       2,     0x02 }; // XUSB
static const eepromEntry_S rmbEeprom_HasEmbeddedDeb         = { 0x0043,      1,       2,     0x03 }; // EDIM
static const eepromEntry_S rmbEeprom_LcdHwBrightMin         = { 0x0044,      1,       2,     0x04 }; // BMIN
static const eepromEntry_S rmbEeprom_LcdHwBrightMax         = { 0x0045,      1,       2,     0x05 }; // BMAX
static const eepromEntry_S rmbEeprom_LcdType                = { 0x0050,      1,       2,     0x10 }; // LCD
// PAGE 3
static const eepromEntry_S rmbEeprom_LruSerialNumber        = { 0x0060,     32,       3,     0x00 }; // S
// PAGE 4
static const eepromEntry_S rmbEeprom_InitialWdTimeout       = { 0x0081,      2,       4,     0x01 }; // T
static const eepromEntry_S rmbEeprom_BootSource             = { 0x0083,      1,       4,     0x03 }; // B
static const eepromEntry_S rmbEeprom_WdResetLimitSec        = { 0x0084,      2,       4,     0x04 }; // W
static const eepromEntry_S rmbEeprom_InitialPwrLedBright    = { 0x0086,      1,       4,     0x06 }; // Y
static const eepromEntry_S rmbEeprom_InitialConLedBright    = { 0x0087,      1,       4,     0x07 }; // Z
static const eepromEntry_S rmbEeprom_LcdSwBrightMin         = { 0x0088,      1,       4,     0x08 }; // PMIN
static const eepromEntry_S rmbEeprom_LcdSwBrightMax         = { 0x0089,      1,       4,     0x09 }; // PMAX
static const eepromEntry_S rmbEeprom_DisableLcd             = { 0x008A,      1,       4,     0x0A }; // D
static const eepromEntry_S rmbEeprom_NeverBootedFlag        = { 0x008B,      1,       4,     0x0B }; // J
static const eepromEntry_S rmbEeprom_EngReservedPage4       = { 0x008C,      2,       4,     0x0C }; // X
static const eepromEntry_S rmbEeprom_MaxFailedBootAttempts  = { 0x008E,      2,       4,     0x0E }; // FBM
static const eepromEntry_S rmbEeprom_CurFailedBootAttempts  = { 0x0090,      2,       4,     0x10 }; // FBC
static const eepromEntry_S rmbEeprom_CurBootState           = { 0x0092,      1,       4,     0x12 }; // BS
static const eepromEntry_S rmbEeprom_BootProgessCode        = { 0x0093,      2,       4,     0x13 }; // BPROG
static const eepromEntry_S rmbEeprom_RebootDelayFlag        = { 0x0095,      1,       4,     0x15 }; // PDLY
// PAGE 5
static const eepromEntry_S rmbEeprom_VoltageStatisBits      = { 0x00A0,      1,       5,     0x00 }; // V
static const eepromEntry_S rmbEeprom_SwResetReason          = { 0x00A1,      1,       5,     0x01 }; // N
static const eepromEntry_S rmbEeprom_EngReservedPage5       = { 0x00A2,      2,       5,     0x02 }; // X
static const eepromEntry_S rmbEeprom_NumEmmc1Failures       = { 0x00A4,      1,       5,     0x04 }; // E1F
static const eepromEntry_S rmbEeprom_NumRjuBusFailures      = { 0x00A5,      1,       5,     0x05 }; // Uc
static const eepromEntry_S rmbEeprom_Sd1ErrorCount          = { 0x00A6,      1,       5,     0x06 }; // SE1
static const eepromEntry_S rmbEeprom_Sd2ErrorCount          = { 0x00A7,      1,       5,     0x07 }; // SE2
static const eepromEntry_S rmbEeprom_RduRebootCoun          = { 0x00A8,      1,       5,     0x08 }; // RB
static const eepromEntry_S rmbEeprom_ExpectedSdCardCount    = { 0x00A9,      1,       5,     0x09 }; // SC
static const eepromEntry_S rmbEeprom_EtcResetEntry1         = { 0x00AA,      4,       5,     0x0A }; // E1
static const eepromEntry_S rmbEeprom_EtcResetEntry2         = { 0x00B0,      4,       5,     0x10 }; // E2
static const eepromEntry_S rmbEeprom_EtcResetentry3         = { 0x00B4,      4,       5,     0x14 }; // E3
static const eepromEntry_S rmbEeprom_EtcResetentry4         = { 0x00B8,      4,       5,     0x18 }; // E4
static const eepromEntry_S rmbEeprom_EtcResetEntry5         = { 0x00BC,      4,       5,     0x1C }; // E5
// PAGE 6
static const eepromEntry_S rmbEeprom_BoardInitialDom        = { 0x00C0,     32,       6,     0x00 }; // BD
// PAGE 7
static const eepromEntry_S rmbEeprom_BoardPartNumber        = { 0x00E0,     32,       7,     0x00 }; // BP
// PAGE 8
static const eepromEntry_S rmbEeprom_BoardSerialNumber      = { 0x0100,     32,       8,     0x00 }; // BS
// PAGE 9
static const eepromEntry_S rmbEeprom_BoardInitialRev        = { 0x0120,     32,       9,     0x00 }; // BR
// PAGE 10
static const eepromEntry_S rmbEeprom_BoardCurrentRev        = { 0x0140,     32,      10,     0x00 }; // BRU
// PAGE 11
static const eepromEntry_S rmbEeprom_BoardCurrentDom        = { 0x0160,     32,      11,     0x00 }; // BDU
// PAGE 12
static const eepromEntry_S rmbEeprom_MacAddr100Mbit         = { 0x0180,      6,      12,     0x00 }; // MM
static const eepromEntry_S rmbEeprom_MacAddr1Gbit           = { 0x0190,      6,      12,     0x10 }; // MG
// PAGE 13
static const eepromEntry_S rmbEeprom_MacAddr100MbitBackup   = { 0x01A0,      6,      13,     0x00 }; // MMB
static const eepromEntry_S rmbEeprom_MacAddr1GbitBackup     = { 0x01B0,      6,      13,     0x10 }; // MGB
// PAGE 50 to 51
static const eepromEntry_S rmbEeprom_SwEtcElapsed           = { 0x0640,     84,      50,     0x00 }; // MMB
// PAGE 52 (some data still for ETC)
static const eepromEntry_S rmbEeprom_SwEtcPwrDwn            = { 0x069C,      4,      52,     0x14 }; // MGB
// PAGE 54 to 62
static const eepromEntry_S rmbEeprom_EncryptTestMsgPri      = { 0x06C0,    288,      54,     0x00 }; //
// PAGE 63
static const eepromEntry_S rmbEeprom_ExchangeKeyInfoPri     = { 0x07E0,     32,      63,     0x00 }; //
// PAGE 64 to 127
static const eepromEntry_S rmbEeprom_ExchangeKeyPri         = { 0x0800,   2048,      64,     0x00 }; //
// PAGE 438 to 466
static const eepromEntry_S rmbEeprom_EncryptTestMsgSec      = { 0x36C0,    288,     438,     0x00 }; //
// PAGE 447
static const eepromEntry_S rmbEeprom_ExchangeKeyInfoSec     = { 0x37E0,     32,     447,     0x00 }; //
// PAGE 448 to 511
static const eepromEntry_S rmbEeprom_ExchangeKeySec         = { 0x3800,   2048,     488,     0x00 }; //


////////////////
// DEB EEPROM
////////////////
static const eepromEntry_S debEeprom_LruDom              = { 0x10,     16,       0,     0x10 }; // D
static const eepromEntry_S debEeprom_LruPartNumber       = { 0x20,     32,       1,     0x00 }; // P
static const eepromEntry_S debEeprom_LruSerialNumber     = { 0x40,     32,       2,     0x00 }; // S
static const eepromEntry_S debEeprom_DebGeneration       = { 0xC5,      1,       6,     0x05 }; // DG
static const eepromEntry_S debEeprom_BoardPartNumber     = { 0x120,    32,      10,     0x00 }; // AP
static const eepromEntry_S debEeprom_BoardSerialNumber   = { 0x140,    32,      11,     0x00 }; // AS
static const eepromEntry_S debEeprom_BoardInitialDom     = { 0x160,    32,      12,     0x00 }; // AD
static const eepromEntry_S debEeprom_BoardInitialRev     = { 0x180,    32,      13,     0x00 }; // DR
static const eepromEntry_S debEeprom_BoardCurrentRev     = { 0x1A0,    32,      14,     0x00 }; // DRU
static const eepromEntry_S debEeprom_BoardCurrentDom     = { 0x1C0,    32,      15,     0x00 }; // DDU

static const eepromEntry_S niEeprom_SwitchPortLeft       = { 0x80,     4,       4,      0x00 }; // T0,L0,V0,D0
static const eepromEntry_S niEeprom_SwitchPortRight      = { 0x84,     4,       4,      0x04 }; // T1,L1,V1,D1
static const eepromEntry_S niEeprom_SwitchPort1          = { 0x88,     4,       4,      0x08 }; // T2,L2,V2,D2
static const eepromEntry_S niEeprom_SwitchPort2          = { 0x8C,     4,       4,      0x0C }; // T3,L3,V3,D3
static const eepromEntry_S niEeprom_SwitchPort3          = { 0x90,     4,       4,      0x10 }; // T4,L4,V4,D4
static const eepromEntry_S niEeprom_SwitchPort4          = { 0x94,     4,       4,      0x14 }; // T5,L5,V5,D5
static const eepromEntry_S niEeprom_SwitchPortLeftCfg    = { 0x98,     1,       4,      0x18 }; // NCl
static const eepromEntry_S niEeprom_SwitchPortRightCfg   = { 0x99,     1,       4,      0x19 }; // NCr
static const eepromEntry_S niEeprom_SwitchPort1Cfg       = { 0x9A,     1,       4,      0x1A }; // NC1
static const eepromEntry_S niEeprom_SwitchPort2Cfg       = { 0x9B,     1,       4,      0x1B }; // NC2
static const eepromEntry_S niEeprom_SwitchPort3Cfg       = { 0x9C,     1,       4,      0x1C }; // NC3
static const eepromEntry_S niEeprom_SwitchPort4Cfg       = { 0x9D,     1,       4,      0x1D }; // NC4

typedef struct __attribute__ ((packed)) {
    uint8_t sdu_type;
    uint8_t location_type;
    uint8_t numeric_value;
    uint8_t ascii_descriptor;
} PortEntry_S;

#endif // ZII_PIC_RDU2_EEPROM_MAP_H
