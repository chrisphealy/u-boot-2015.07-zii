
#include <common.h>
#include <command.h>
#include <asm/errno.h>

#include <asm/errno.h>

DECLARE_GLOBAL_DATA_PTR;

//-----------------------------------------------------------------------

//--------------------------
// Global Variables
//--------------------------

//--------------------------
// Local Function Prototypes
//--------------------------
void get_zii_board_mac_addr(int nic_id, char mac_addr[6]);

//------------------------------
// External Function Prototypes
//------------------------------
extern void get_mx6_board_mac_addr(int nic_id, char mac_addr[6]);

void get_zii_board_mac_addr(int nic_id, char mac_addr[6])
{
#if defined(CONFIG_TARGET_ZII_MX6Q_RDU2)
	get_mx6_board_mac_addr(nic_id, mac_addr);
#elif defined(CONFIG_TARGET_ZII_VF610_DEV)
	printf("Vybrid not yet implemented\n");
#else
	printf("Unknown processor type\n");
#endif
}

int cmd_zii_get_mac (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
    int nic_id  = 0;
    char mac_addr[6] = {0};

    // Check the number on command line arguments
    if (argc != 2) {
        cmd_usage(cmdtp);
        return 1;
    }

    nic_id  = simple_strtol(argv[1], NULL, 10);

    get_zii_board_mac_addr(nic_id, mac_addr);

    return 0;
}

U_BOOT_CMD(
    zii_get_mac ,    2,    0,     cmd_zii_get_mac,
    "Get MAC address for specified NIC",
    "zii_get_mac <nic_id>\n"
	""
);


