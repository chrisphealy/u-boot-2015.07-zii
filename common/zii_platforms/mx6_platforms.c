#include <common.h>
#include <command.h>
#include <fuse.h>
#include <environment.h>

#include <asm/io.h>
#include <asm/arch/iomux.h>
#include <asm/errno.h>
#include <i2c.h>
#include <stdbool.h>

#include "../zii_platforms/zii_board_types.h"
#include "../zii_pic_comm/zii_pic_rdu2.h"

DECLARE_GLOBAL_DATA_PTR;

/*
 * Every processor type used by Zii
 *  has 4 dedicated pins for board type.
 * This allows software to detect which type of
 *   i.M51, i.MX6, Vybrid board is running.
 */
typedef enum {
	MX6_TYPE__RDU2		= 0x00,
	MX6_TYPE__UNKNOWN 	= 0xFF  /* All GPIOs are pulled up internally */
} mx6_types_E;

mx6_types_E mx6_type = MX6_TYPE__RDU2;

extern MacErrorCodes_E rdu2_pic_getMacAddress(NicIds_E nic_id, uint8_t mac_addr[6]);

u32 get_mx6_rand_seed(void)
{
	u32 rand_seed = 0;

	/*
	 * i.MX6 Unique ID Lower
	 * Fuse bank: 0
	 * Offset: 2
	 */
	fuse_read(0, 2, &rand_seed);

	return rand_seed;
}


void get_mx6_board_mac_addr(int nic_id, char mac_addr[6])
{
	int valid_mac_found = 0;
	u32 rand_seed = 0;
	char mac_string[18] = {0};

	switch(mx6_type) {
	case MX6_TYPE__RDU2:
		/* There are 2 possible ways to get/create a MAC Address
		 * for the RDU2 Platform
		 * 1. From the RMB EEPROM
		 * 2. Random
		 */

	    /*
	     * First try and get the MAC address from the RMB EEPROM via
	     *   the PIC interface.
	     *  There are 2 MAC Address associated with RDU2:
	     *    1. 100 Mbit i.MX6 FEC
	     *    2. 1Gbit Intel i210 NIC
	     */
		{
			MacErrorCodes_E ret_val = rdu2_pic_getMacAddress((NicIds_E)nic_id, (uint8_t *)mac_addr);
			if(ret_val == MAC_ERROR_PRI_BAK_UNINIT ||
				ret_val == 	MAC_ERROR_PRI_BAK_CRC_BAD ||
				ret_val == MAC_ERROR_UNKNOWN_NIC)
			{
				/* There was an unrecoverable error or a MAC
				 *   address has not been programmed
				 * Set all of the bytes to 0xFF to indicate a problem
				 */
				mac_addr[5] = 0xFF;
				mac_addr[4] = 0xFF;
				mac_addr[3] = 0xFF;
				mac_addr[2] = 0xFF;
				mac_addr[1] = 0xFF;
				mac_addr[0] = 0xFF;

				/*
				 * Get a seed for srand()
				 */
				rand_seed = get_mx6_rand_seed();

				break;
			}
			valid_mac_found = 1;

			sprintf(mac_string, "%02X:%02X:%02X:%02X:%02X:%02X",mac_addr[0], mac_addr[1],
																mac_addr[2], mac_addr[3],
																mac_addr[4], mac_addr[5]);
			if(nic_id == 0) {
				// Integrated FEC
				setenv ("ethaddr",mac_string);
			}
			else {
				// Intel i210 via PCIe
				setenv ("eth1addr",mac_string);
			}
		}
		break;
	default:
		printf("Unknown System Type: ");
		break;
	}

	if(valid_mac_found == 0) {
		/* Create a random MAC address */
		{
			unsigned long ethaddr_low, ethaddr_high;

			/*
			 * Seed srand with a portion of the processors Unique ID
			 */
			srand(rand_seed);

			/*
			 * setting the 2nd LSB in the most significant byte of
			 * the address makes it a locally administered ethernet
			 * address
			 */
			ethaddr_high = (rand() & 0xfeff) | 0x0200;
			ethaddr_low = rand();

			mac_addr[0] = ethaddr_high >> 8;
			mac_addr[1] = ethaddr_high & 0xff;
			mac_addr[2] = ethaddr_low >> 24;
			mac_addr[3] = (ethaddr_low >> 16) & 0xff;
			mac_addr[4] = (ethaddr_low >> 8) & 0xff;
			mac_addr[5] = ethaddr_low & 0xff;
		}

		printf("Created random MAC address: %02X:%02X:%02X:%02X:%02X:%02X\n", mac_addr[0], mac_addr[1],
																				mac_addr[2], mac_addr[3],
																				mac_addr[4], mac_addr[5]);
	}
}
