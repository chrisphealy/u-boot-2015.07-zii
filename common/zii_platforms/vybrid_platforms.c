#include <common.h>
#include <command.h>

typedef enum {
	VYBRID_TYPE__UNKNOWN 	= 0xFF  /* All GPIOs are pulled up internally */
} vybrid_types_E;
