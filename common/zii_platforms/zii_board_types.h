/*
 *  Copyright (C) 2015 Zodiac Inflight Innovations
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __ZII_BOARD_TYPES_H
#define __ZII_BOARD_TYPES_H


/* System Types */
typedef enum {
    SYSTEM_TYPE__RDU2         = 0x00,  // RDU2

    SYSTEM_TYPE__UNRECOGNIZED = 0xff,  /* Unrecognized */
} system_type_E;

#endif /* __ZII_BOARD_TYPES_H */
