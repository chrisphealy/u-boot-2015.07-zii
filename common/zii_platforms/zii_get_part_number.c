
#include <common.h>
#include <environment.h>

DECLARE_GLOBAL_DATA_PTR;

void get_zii_part_number(void);

void get_zii_part_number()
{
#ifdef BUILD_TAG
	setenv ("uboot_part_num", (char *)BUILD_TAG);
#else
	setenv ("uboot_part_num", "UNKNOWN");
#endif
}


