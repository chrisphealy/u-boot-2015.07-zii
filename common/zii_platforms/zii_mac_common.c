
void zii_create_random_mac(u32 rand_seed, char mac_addr[6]) {
	unsigned long ethaddr_low, ethaddr_high;

	/*
	 * Seed srand with a portion of the processors Unique ID
	 */
	srand(rand_seed);

	/*
	 * setting the 2nd LSB in the most significant byte of
	 * the address makes it a locally administered ethernet
	 * address
	 */
	ethaddr_high = (rand() & 0xfeff) | 0x0200;
	ethaddr_low = rand();

	mac_addr[0] = ethaddr_high >> 8;
	mac_addr[1] = ethaddr_high & 0xff;
	mac_addr[2] = ethaddr_low >> 24;
	mac_addr[3] = (ethaddr_low >> 16) & 0xff;
	mac_addr[4] = (ethaddr_low >> 8) & 0xff;
	mac_addr[5] = ethaddr_low & 0xff;
}
