/*
 *  Copyright (C) 2015 Zodiac Inflight Innovations
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <stdbool.h>

#ifndef COMMON_ZII_PLATFORMS_ZII_RDU2_LCD_TYPE_H_
#define COMMON_ZII_PLATFORMS_ZII_RDU2_LCD_TYPE_H_

typedef enum {
    DISPLAY_TYPE_UNKNOWN  = 0,
	DISPLAY_TYPE_AUO_10_1 = 1,
	DISPLAY_TYPE_NEC_12_1 = 2,
	DISPLAY_TYPE_NLT_11_6 = 3,
	DISPLAY_TYPE_AUO_13_3 = 4,
	DISPLAY_TYPE_AUO_18_5 = 5,
    DISPLAY_TYPE_COUNT
} Display_Types_E;

typedef enum {
	INTERFACE_TYPE__SINGLE_LVDS = 0,
	INTERFACE_TYPE__DUAL_LVDS,
	INTERFACE_TYPE__EDP
} Display_Interface_Type_E;

typedef struct {
	char * display_name;
	Display_Interface_Type_E interface_type;
} Display_Data_S;


static const Display_Data_S displayData[] = {
										/* DEFAULT/UNKNOWN Innolux 10.1" */
										{ "innolux_10_1",
										INTERFACE_TYPE__SINGLE_LVDS },

										/* Innolux 10.1" */
										{ "innolux_10_1",
										INTERFACE_TYPE__SINGLE_LVDS },

										/* NEC 12.1" */
										{ "nec_12_1",
										INTERFACE_TYPE__SINGLE_LVDS },

										/* NLT 11.6" */
										{ "nlt_11_6",
										INTERFACE_TYPE__EDP },

										/* AUO 13.3" */
										{ "auo_13_3",
										INTERFACE_TYPE__DUAL_LVDS },

										/* AUO 18.5" */
										{ "auo_18_5",
										INTERFACE_TYPE__DUAL_LVDS }
									};


#endif /* COMMON_ZII_PLATFORMS_ZII_RDU2_LCD_TYPE_H_ */
