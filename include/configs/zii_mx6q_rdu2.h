/*
 *  Copyright (C) 2015 Zodiac Inflight Innovations
 *
 * Based on:
 * Copyright (C) 2012 Freescale Semiconductor, Inc.
 *
 * Configuration settings for the Freescale i.MX6Q SabreAuto board.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __ZII_MX6Q_RDU2_CONFIG_H
#define __ZII_MX6Q_RDU2_CONFIG_H

#include "mx6_common.h"

#ifdef CONFIG_SPL
#define CONFIG_SPL_LIBCOMMON_SUPPORT
#define CONFIG_SPL_MMC_SUPPORT
#include "imx6_spl.h"
#endif

#define CONFIG_MACH_TYPE	3980
#define CONFIG_MXC_UART_BASE	UART1_BASE
#define CONFIG_CONSOLE_DEV		"ttymxc0"
#define CONFIG_MMCROOT			"/dev/mmcblk0p2"
#define CONFIG_DEFAULT_FDT_FILE	"imx6q-sabresd.dtb"
#define PHYS_SDRAM_SIZE		(2u * 1024 * 1024 * 1024) /* 2GB DDR3 */

#define CONFIG_SUPPORT_EMMC_BOOT /* eMMC specific */

#define CONFIG_ZII_RDU2_PIC_SERIAL_INDEX 1

/* Enabled for video pattern test */
#define CONFIG_CMD_BMP
/* Enabled for LM75 test */
#define CONFIG_DTT_LM75

/* USB Configs */
#define CONFIG_CMD_USB
#define CONFIG_USB_EHCI
#define CONFIG_USB_EHCI_MX6
#define CONFIG_USB_STORAGE
#define CONFIG_USB_HOST_ETHER
#define CONFIG_USB_MAX_CONTROLLER_COUNT 2
#define CONFIG_EHCI_HCD_INIT_AFTER_RESET	/* For OTG port */
#define CONFIG_MXC_USB_PORTSC	(PORT_PTS_UTMI | PORT_PTS_PTW)
#define CONFIG_MXC_USB_FLAGS	0

#define CONFIG_PCA953X
#define CONFIG_SYS_I2C_PCA953X_WIDTH	{ {0x30, 8}, {0x32, 8}, {0x34, 8} }




#define CONFIG_IMX6_THERMAL

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN		(10 * SZ_1M)

#define CONFIG_BOARD_EARLY_INIT_F
#define CONFIG_BOARD_LATE_INIT

#define CONFIG_MXC_UART

#define CONFIG_CMD_FUSE
#if defined(CONFIG_CMD_FUSE) || defined(CONFIG_IMX6_THERMAL)
#define CONFIG_MXC_OCOTP
#endif

/* MMC Configs */
#define CONFIG_SYS_FSL_ESDHC_ADDR      0

#define CONFIG_CMD_PING
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_MII
#define CONFIG_FEC_MXC
#define CONFIG_MII
#define IMX_FEC_BASE			ENET_BASE_ADDR
#define CONFIG_FEC_XCV_TYPE		RMII
#define CONFIG_ETHPRIME			"FEC"
#define CONFIG_FEC_MXC_PHYADDR		0

#define CONFIG_PHYLIB
#define CONFIG_PHY_MICREL

#define CONFIG_CMD_SF
#ifdef CONFIG_CMD_SF
#define CONFIG_SPI_FLASH_STMICRO
#define CONFIG_MXC_SPI
#define CONFIG_SF_DEFAULT_BUS		1
#define CONFIG_SF_DEFAULT_CS		0
#define CONFIG_SF_DEFAULT_SPEED		20000000
#define CONFIG_SF_DEFAULT_MODE		SPI_MODE_0
#endif


/* SATA Configs */
#define CONFIG_CMD_SATA
#ifdef CONFIG_CMD_SATA
#define CONFIG_DWC_AHSATA
#define CONFIG_SYS_SATA_MAX_DEVICE  1
#define CONFIG_DWC_AHSATA_PORT_ID   0
#define CONFIG_DWC_AHSATA_BASE_ADDR SATA_ARB_BASE_ADDR
#define CONFIG_LBA48
#define CONFIG_LIBATA
#endif

/* Command definition */
#define CONFIG_CMD_BMODE

#ifdef CONFIG_SUPPORT_EMMC_BOOT
#define EMMC_ENV \
	""
#else
#define EMMC_ENV ""
#endif

#define CONFIG_EXTRA_ENV_SETTINGS \
	"bootdelay=2\0" \
	"baudrate=115200\0" \
	"bootargs_emmc_OS1=setenv bootargs console=${console},${baudrate} " \
		"root=/dev/mmcblk3p1 rootwait rw earlyprintk ip=${ipsetup} uboot_partno=${uboot_part_num}\0" \
	"bootargs_emmc_OS2=setenv bootargs console=${console},${baudrate} " \
		"root=/dev/mmcblk3p2 rootwait rw earlyprintk ip=${ipsetup} uboot_partno=${uboot_part_num}\0" \
	"bootcmd_emmc_OS1=pic_get_ip_addr; run bootargs_emmc_OS1; mmc dev 2; mmc read ${loadaddr} ${kernel_offset_1} ${kernel_size}; " \
		"mmc read ${fdt_addr} ${dtb_offset_1} ${dtb_size}; bootm ${loadaddr} - ${fdt_addr}\0" \
	"bootcmd_emmc_OS2=pic_get_ip_addr; run bootargs_emmc_OS2; mmc dev 2; mmc read ${loadaddr} ${kernel_offset_2} ${kernel_size}; " \
		"mmc read ${fdt_addr} ${dtb_offset_2} ${dtb_size}; bootm ${loadaddr} - ${fdt_addr}\0" \
	"console=" CONFIG_CONSOLE_DEV "\0" \
	"dtb_offset_1=0x0800\0" \
	"dtb_offset_2=0x7800\0" \
	"dtb_size=0x00C8\0" \
	"ethact=FEC\0" \
	"ethprime=FEC\0" \
	"fdt_addr=0x18000000\0" \
	"fdt_high=0xffffffff\0" \
	"image=uImage\0" \
	"initrd_high=0xffffffff\0" \
	"kernel_offset_1=0x2800\0" \
	"kernel_offset_2=0x9800\0" \
	"kernel_size=0x3400\0" \
	"loadaddr=0x12000000\0"

#define CONFIG_BOOTCOMMAND \
	"run bootcmd_emmc_OS1"

#define CONFIG_ARP_TIMEOUT     200UL

#define CONFIG_SYS_MEMTEST_START       0x10000000
#define CONFIG_SYS_MEMTEST_END         0x10010000
#define CONFIG_SYS_MEMTEST_SCRATCH     0x10800000

#define CONFIG_STACKSIZE               (128 * 1024)

/* Physical Memory Map */
#define CONFIG_NR_DRAM_BANKS           1
#define PHYS_SDRAM                     MMDC0_ARB_BASE_ADDR

#define CONFIG_SYS_SDRAM_BASE          PHYS_SDRAM
#define CONFIG_SYS_INIT_RAM_ADDR       IRAM_BASE_ADDR
#define CONFIG_SYS_INIT_RAM_SIZE       IRAM_SIZE

#define CONFIG_SYS_INIT_SP_OFFSET \
	(CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
	(CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)

/* Environment organization */
#define CONFIG_ENV_SIZE			(16 * 1024)
#define CONFIG_ENV_SIZE_REDUND  (16 * 1024)

#define CONFIG_ENV_IS_IN_MMC

#if defined(CONFIG_ENV_IS_IN_MMC)
#  define CONFIG_ENV_OFFSET			(768 * 1024)
#  define CONFIG_ENV_OFFSET_REDUND	(818 * 1024)
#  define CONFIG_SYS_REDUNDAND_ENVIRONMENT
#endif

#define CONFIG_ENV_IS_IN_JTAG
/*#define CONFIG_ENV_JTAG_USE_BUILT_IN_VERSION*/
#if defined(CONFIG_ENV_IS_IN_JTAG)
#  define CONFIG_ENV_JTAG_OFFSET		0x12000000
#endif

/* Framebuffer */
#define CONFIG_VIDEO
#define CONFIG_VIDEO_IPUV3
#define CONFIG_CFB_CONSOLE
#define CONFIG_VGA_AS_SINGLE_DEVICE
#define CONFIG_SYS_CONSOLE_IS_IN_ENV
#define CONFIG_SYS_CONSOLE_OVERWRITE_ROUTINE
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_IPUV3_CLK 260000000
#define CONFIG_IMX_VIDEO_SKIP

#ifndef CONFIG_SPL
#define CONFIG_CI_UDC
#define CONFIG_USBD_HS
#define CONFIG_USB_GADGET_DUALSPEED

#define CONFIG_USB_GADGET
#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USBDOWNLOAD_GADGET
#define CONFIG_USB_GADGET_VBUS_DRAW	2

#define CONFIG_G_DNL_VENDOR_NUM		0x0525
#define CONFIG_G_DNL_PRODUCT_NUM	0xa4a5
#define CONFIG_G_DNL_MANUFACTURER	"FSL"
#endif


#define CONFIG_SYS_FSL_USDHC_NUM	3
#if defined(CONFIG_ENV_IS_IN_MMC)
#define CONFIG_SYS_MMC_ENV_DEV		2	/* SDHC3 */
#endif

/* Rmoving PCIe as a quick fix to a strange reboot problem */
/*#define CONFIG_CMD_PCI*/
#ifdef CONFIG_CMD_PCI
#define CONFIG_E1000
#define CONFIG_PCI
#define CONFIG_PCI_PNP
#define CONFIG_PCI_SCAN_SHOW
#define CONFIG_PCIE_IMX
#define CONFIG_PCIE_IMX_PERST_GPIO	IMX_GPIO_NR(7, 12)
#define CONFIG_PCIE_IMX_POWER_GPIO	IMX_GPIO_NR(3, 19)
#endif

/* I2C Configs */
#define CONFIG_CMD_I2C
#define CONFIG_SYS_I2C
#define CONFIG_SYS_I2C_MXC
#define CONFIG_SYS_I2C_MXC_I2C1		/* enable I2C bus 1 */
#define CONFIG_SYS_I2C_MXC_I2C2		/* enable I2C bus 2 */
#define CONFIG_SYS_I2C_MXC_I2C3		/* enable I2C bus 3 */
#define CONFIG_SYS_I2C_SPEED		400000

/* PMIC */
#define CONFIG_POWER
#define CONFIG_POWER_I2C
#define CONFIG_POWER_PFUZE100
#define CONFIG_POWER_PFUZE100_I2C_ADDR	0x08

#define CONFIG_OF_BOARD_SETUP

#endif /* __ZII_MX6Q_RDU2_CONFIG_H */
